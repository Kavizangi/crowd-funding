<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>Login</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/')?>">Home</a><span>/</span></li>
					<li>Login</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="container">
		<div class="main-content">
			<div class="form-login">
				<h2>Log in with your account</h2>
				<?php
				if ( !empty($this->session->flashdata('login_fail')) ){
				?>
					<div class="alert alert-danger">
						<?php echo $this->session->flashdata('login_fail');?>
					</div>
				<?php
				}
				?>
				<form action="<?php echo base_url('auth/index');?>" method="POST" id="loginForm" class="clearfix">
					<div class="field">
						<input type="email" name="email" placeholder="Email" required/>
					</div>
					<div class="field">
						<input type="password" name="password" placeholder="Password" required/>
					</div>
					<div class="inline clearfix">
						<button type="submit" value="Send Messager" class="btn-primary">Login</button>
						<p>Not a member yet? <a href="<?php echo base_url('auth/register')?>">Register Now</a></p>
					</div>
				</form>
			</div>
		</div>
	</div>
	<br/>
	<br/>
	<br/>
</main><!-- .site-main -->
