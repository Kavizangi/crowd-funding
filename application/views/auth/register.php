<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>Register</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li>Register</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="container">
		<div class="main-content">
			<div class="form-login form-register">
				<h2>Register for Free</h2>
				<?php
				if ( !empty($this->session->flashdata('login_fail')) ){
				?>
					<div class="alert alert-danger">
						<?php echo $this->session->flashdata('login_fail');?>
					</div>
				<?php
				}elseif (!empty($this->session->flashdata('login_success'))){
				?>
					<div class="alert alert-danger">
						<?php echo $this->session->flashdata('login_success');?>
					</div>
				<?php
				}
				?>
				<form action="<?php echo base_url('auth/register')?>" method="POST" id="registerForm" class="clearfix">
					<div class="field">
						<input type="text" value="<?php echo set_value('first_name');?>" name="first_name" placeholder="First Name" required/>
					</div>
					<div class="field">
						<input type="text" value="<?php echo set_value('last_name');?>" name="last_name" placeholder="Last Name" required/>
					</div>
					<div class="field">
						<input type="email" value="<?php echo set_value('email');?>" name="email" placeholder="E-mail Address" />
					</div>
					<div class="field">
						<input type="text" value="<?php echo set_value('mobile_no');?>" name="mobile_no" placeholder="Mobile No." />
					</div>
					<div class="field">
						<input type="password" value="" name="password" placeholder="Password" />
					</div>
					<div class="field">
						<input type="password" value="" name="confirm_password" placeholder="Confirm Password" />
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<br /><p>
								<input style="width:auto;height:auto;" type="checkbox" name="agree" value="checkbox" id="agreeId" required /> I have reviewed and agree to the <span><a style="display:inline-block" target="_blank" href="http://peexc.com//terms/"> Terms of Service</a></span> and <span><a style="display:inline-block" target="_blank" href="http://peexc.com//privacy/"> Privacy Policy</a></span></p>
						</div><br />
					</div>
					<div class="inline clearfix">
						<button type="submit" value="Register Account" class="btn-primary">Register Account</button>
						<p>Not a member yet? <a href="<?php echo base_url('auth/login');?>">Login Now</a></p>
					</div>
				</form>
			</div>
		</div>
	</div>
	<br/>
	<br/>
	<br/>
</main><!-- .site-main -->
