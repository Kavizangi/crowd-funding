<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>My Campaigns</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li>My Campaigns</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="account-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="campaign-content">
						<div class="row">
							<?php
								$campaigns = $this->db->where('user_id',$this->session->userdata('eqty_userid'))->get('campaigns')->result();
								if (empty($campaigns)){
							?>
							<div class="col-md-12">
								<div class="alert alert-info">
									<i class="fa fa-info"></i> You do not have any campaigns yet, create one
								</div>
							</div>
							<?php
								}else {
									foreach ( $campaigns as $campaign ) {
										$images = json_decode($campaign->images);
										$firstImage = $images[0];
										$author = $this->db->where('id',$campaign->user_id)->get('register')->row();
										$category = $this->db->where('id',$campaign->category)->get('category')->row();
							?>
										<div class="col-lg-4 col-md-6 col-sm-6 col-6">
											<div class="campaign-item">
												<a class="overlay" href="<?php echo base_url('home/campaignDetails/'.$campaign->id); ?>">
													<img src="<?php echo base_url($firstImage);?>" alt="">
													<span class="ion-ios-search-strong"></span>
												</a>
												<div class="campaign-box">
													<a href="#" class="category"><?php echo !empty($category->catagory_name) ? $category->catagory_name : ""?></a>
													<h3><a href="<?php echo base_url('home/campaignDetails/'.$campaign->id); ?>">
															<?php echo $campaign->title;?></a></h3>
													<div class="campaign-description">
														<?php
															echo $campaign->introduction;
														?>
													</div>
													<div class="campaign-author">
														<a class="author-icon" href="#"><img src="<?php echo !empty($author->profile_image) ?  base_url("uploads/user-profile/".$author->profile_image) : base_url('assets/images/dashboard-avatar.png'); ?>" alt=""></a>by <a
															class="author-name" href="#"><?php echo $author->firstname.' '.$author->lastname;?></a></div>
													<div class="process">
														<div class="raised"><span></span></div>
														<div class="process-info">
															<div class="process-pledged">
																<span>
																	<?php echo $data['settings']['site_currency'].' '.number_format($campaign->raise_amount);?>
																</span>
																Target
															</div>
															<div class="process-funded"><span><?php echo $data['settings']['site_currency'].' 0';?></span>Funded</div>
															<div class="process-time"><span>
																	<?php
																		$created_on = strtotime($campaign->created_on);
																		$now = time();

																		$date_diff = $now - $created_on;
																	    $days = round($date_diff / (60 * 60 * 24));

																	    echo ($campaign->duration - $days);
																	?>
																</span>Days left</div>
														</div>
													</div>
												</div>
											</div>
										</div>
							<?php
									}
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</div><!-- .container -->
	</div><!-- .page-content -->
</main><!-- .site-main -->
