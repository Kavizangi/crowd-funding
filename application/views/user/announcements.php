<?php
	$company_announcements = $this->db->where('user_id',$this->session->userdata('eqty_userid'))->get('company_announcements')->result();
?>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>Announcements</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li>Announcements</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="account-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="account-content account-table">
						<h3 class="account-title">Announcements
							<button data-toggle="modal" data-target="#announcementModal" class="btn btn-primary btn-table-sm pull-right">
								<i class="fa fa-plus"></i> Add
							</button>
							<br/>
							<br/>
						</h3>

						<?php
						if ( !empty($this->session->flashdata('crud_announcements_success')) ){
							?>
							<div class="alert alert-success">
								<?php echo $this->session->flashdata('crud_announcements_success');?>
							</div>
							<?php
						}else if ( !empty($this->session->flashdata('crud_announcements_failed')) ){
							?>
							<div class="alert alert-danger">
								<?php echo $this->session->flashdata('crud_announcements_failed');?>
							</div>
							<?php
						}
						?>

						<div class="account-main">
							<table class="table table-responsive dash_table">
								<thead>
								<tr>
									<th>#</th>
									<th>Content</th>
									<th>Updated On</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								<?php

								$i=1;
								if (!empty($company_announcements)) {
									foreach ($company_announcements as $datum) {
										?>
										<tr>
											<td><?php echo $i++;?></td>
											<td><?php echo $datum->content;?></td>
											<td><?php echo $datum->updated_on;?></td>
											<td><?php echo $datum->status == 1 ? "Visible" : "Hidden";?></td>
											<td>
												<button data-toggle="modal" data-target="#announcementModal<?php echo $i;?>" class="btn btn-primary btn-table-sm">
													<i class="fa fa-eye">Edit</i>
												</button>

												<a href="<?php echo base_url('user/delete/announcement/'.$datum->id)?>" onclick="return confirm('Are you sure to delete?');" class="btn btn-danger"><i class="fa fa-trash"></i> Delete </a>

												<!-- Modal -->
												<div class="modal fade" id="announcementModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="announcementModal" aria-hidden="true">
													<div class="modal-dialog modal-lg" role="document">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel">Edit Announcement</h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<div class="row">
																	<div class="col-md-12">
																		<form method="post" enctype="multipart/form-data" action="">
																			<input type="hidden" name="id" value="<?php echo $datum->id;?>"/>
																			<input type="hidden" name="action" value="edit"/>
																			<div class="row">
																				<div class="col-md-12">
																					<div class="field">
																						<label for="content">Announcement</label>
																						<textarea id="content<?php echo $datum->id;?>" name="content"><?php echo $datum->content;?></textarea>
																					</div>
																				</div>
																			</div>

																			<div class="row">
																				<div class="col-md-12">
																					<div class="field">
																						<label for="content">Status</label>
																						<select name="status">
																							<option <?php echo $datum->status == 0 ? "selected" : "";?> value="0">Hidden</option>
																							<option <?php echo $datum->status == 1 ? "selected" : "";?> value="1">Visible</option>
																						</select>
																					</div>
																				</div>
																			</div>

																			<div class="row">
																				<div class="col-md-12">
																					<button type="submit" name="save_action" class="btn-primary">Save Changes</button>
																				</div>
																			</div>
																		</form>
																	</div>

																	<script>
																		CKEDITOR.replace( 'content<?php echo $datum->id;?>' );
																	</script>

																</div>
															</div>
														</div>
													</div>
												</div>
											</td>
										</tr>

										<?php
									}
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- .container -->
	</div><!-- .page-content -->

	<!-- Modal -->
	<div class="modal fade" id="announcementModal" tabindex="-1" role="dialog" aria-labelledby="announcementModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Create Director</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<form method="post" enctype="multipart/form-data" action="">
								<div class="row">
									<div class="col-md-12">
										<div class="field">
											<label for="content">Announcement</label>
											<textarea id="content" name="content" required></textarea>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<div class="field">
											<label for="content">Status</label>
											<select name="status">
												<option value="0">Hidden</option>
												<option value="1">Visible</option>
											</select>
										</div>
									</div>
								</div>

								<div class="col-md-12">
									<button type="submit" name="save_action" class="btn-primary">Save Changes</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main><!-- .site-main -->

<script>
	CKEDITOR.replace( 'content' );
</script>

