<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>My Investments</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li>My Investments</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="account-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="account-content account-table">
						<h3 class="account-title">My Investments</h3>
						<div class="account-main">
							<table class="table table-responsive dash_table">
								<thead>
								<tr>
									<th>S.No.</th>
									<th>Project </th>
									<th>Invest Amount<br /> (In <?php echo $data['settings']['site_currency'];?>)</th>
									<th>Payout <br />Method</th>
									<th>Invest <br />Date</th>
									<th>Pay Status</th>
								</tr>
								</thead>
								<tbody>
								<?php $i=1;

								$payments=$this->db->where('user_id',$this->session->userdata('eqty_userid'))->get('contribute')->result();
								foreach ($payments as $payment) {
									$project = $this->db->where('id',$payment->prjt_id)->get('campaigns')->row();
									?>
									<tr>
										<td><?php echo $i++;?></td>
										<td><?php echo !empty($project) ? $project->title : "<span class='badge badge-dark'>Removed</span>";?> </td>
										<td><?php echo $payment->contribute_amount;?></td>
										<td></td>
										<td><?php echo $payment->date;?></td>
										<td><?php echo $payment->pay_status==1 ? "Paid" : "Processing";?></td>
									</tr>
									<?php
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div><!-- .container -->
		</div><!-- .page-content -->
</main><!-- .site-main -->
