<?php
	$user = $this->Auth_model->getLoggedInUserData();
?>

<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>My Account Profile</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li>My Account Profile</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="account-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="account-content profile">
									<h3 class="account-title">My Profile</h3>
									<?php
										if (!empty($this->session->flashdata('update_profile_success'))){
									?>
										<div class="alert alert-success">
											<?php echo $this->session->flashdata('update_profile_success');?>
										</div>
									<?php
										}else if (!empty($this->session->flashdata('update_profile_failed'))){
									?>
											<div class="alert alert-danger">
												<?php echo $this->session->flashdata('update_profile_failed');?>
											</div>
									<?php
										}
									?>
									<div class="account-main">
										<div class="author clearfix">
											<a class="author-avatar" href="#"><img src="<?php echo !empty($user->profile_image) ?  base_url("uploads/user-profile/".$user->profile_image) : base_url('assets/images/dashboard-avatar.png'); ?>" alt="Profile"></a>
											<div class="author-content">
												<div class="author-title"><h3><a href="#"><?php echo $user->firstname.' '.$user->lastname;?></a></h3></div>
												<div class="author-info">
													<p><?php echo $user->email;?></p>
													<p>Member since <?php echo date('M Y',strtotime($user->crcdt));?></p>
												</div>
											</div>
										</div>
										<div class="profile-box">
											<h3>Personal Details</h3>
											<ul>
												<li>
													<strong>Email</strong>
													<div class="profile-text"><p><?php echo $user->email;?></p></div>
												</li>
												<li>
													<strong>Country:</strong>
													<div class="profile-text">
														<p>
															<?php
																$user_country = $this->db->where('country_id',$user->country)->get('country')->row();
																echo !empty($user_country) ? $user_country->country_name : '';
															?>
														</p>
													</div>
												</li>
												<li>
													<strong>State</strong>
													<div class="profile-text">
														<p>
															<?php
																$user_state = $this->db->where('state_id',$user->state)->get('state')->row();
																echo !empty($user_state) ? $user_state->state_name : '';
															?>
														</p>
													</div>
												</li>
												<li>
													<strong>City:</strong>
													<div class="profile-text">
														<p>
															<?php
															$user_city = $this->db->where('city_id',$user->state)->get('city')->row();
															echo !empty($user_city) ? $user_city->city_name : '';
															?>
														</p>
													</div>
												</li>
												<li>
													<strong>Zipcode:</strong>
													<div class="profile-text"><p><?php echo $user->zipcode;?></p></div>
												</li>
												<li>
													<strong>Phone Number:</strong>
													<div class="profile-text"><p><?php echo $user->phone_code.' '.$user->phone;?></p></div>
												</li>
												<li>
													<strong>Website:</strong>
													<div class="profile-text"><p><a href="website" target="_blank"><?php echo $user->website;?></a></p></div>
												</li>
												<li>
													<strong>Facebook:</strong>
													<div class="profile-text"><p><a href="<?php echo $user->fb_url;?>" target="_blank"><?php echo $user->fb_url;?></a></p></div>
												</li>
												<li>
													<strong>Twitter:</strong>
													<div class="profile-text"><p><a href="<?php echo $user->twitter_url;?>" target="_blank"><?php echo $user->twitter_url;?></a></p></div>
												</li>
												<li>
													<strong>LinkedIn:</strong>
													<div class="profile-text"><p><a href="<?php $user->lnkdin_url;?>" target="_blank"><?php echo $user->lnkdin_url;?></a></p></div>
												</li>
											</ul>
										</div>
										<button class="btn-primary" data-toggle="modal" data-target="#editProfileModal">Edit Profile</button>
										<!-- Modal -->
										<div class="modal fade" id="editProfileModal" tabindex="-1" role="dialog" aria-labelledby="editProfileModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
													</div>
													<div class="modal-body">
														<div class="row">
															<div class="col-md-12">
																<form method="post" class="row" action="<?php echo base_url('user/companyProfile')?>">
																	<div class="col-md-12">
																		<h4>Personal Details</h4>
																		<hr/>
																	</div>
																	<div class="col-md-12">
																		<div class="row">
																			<div class="col-md-6">
																				<div class="form-group">
																					<label>First Name</label>
																					<input type="text" name="f_name" class="form-control" value="<?php echo $user->firstname;?>" required />
																				</div>
																			</div>

																			<div class="col-md-5">
																				<div class="form-group">
																					<label>Last Name</label>
																					<input type="text" name="l_name" class="form-control" value="<?php echo $user->lastname;?>" required />
																				</div>
																			</div>
																		</div>
																	</div>

																	<div class="col-md-12">
																		<div class="row">
																			<div class="col-md-6">
																				<div class="form-group">
																					<label>Email</label>
																					<input type="email" class="form-control" value="<?php echo $user->email;?>" readonly />
																				</div>
																			</div>
																			<div class="col-md-5">
																				<div class="form-group">
																					<label class="col-sm-3">Country</label>
																					<select name="ctry_name" id="country" class="form-control" onchange="get_state(this.value)" required>
																						<option value=""> Select Country </option>
																						<?php
																							$countries = $this->db->get('country')->result();
																							foreach ($countries as $country) {
																						?>
																							<option <?php echo $country->country_id == $user->country ? 'selected' : '';?> value="<?php echo $country->country_id;?>"><?php echo $country->country_name;?> </option>
																						<?php
																							}
																						?>
																					</select>
																				</div>
																			</div>
																		</div>
																	</div>

																	<div class="col-md-12">
																		<div class="row">
																			<div class="col-md-6">
																				<div class="form-group">
																					<label>State</label>
																					<select name="st_name" id="showstate" onchange="get_city(this.value)" class="form-control">
																						<option value=""> Select State </option>
																						<?php
																						$states = $this->db->where('state_country_id',$user->country)->get('state')->result();
																						if (!empty($states)){
																							foreach ($states as $state) {
																						?>
																							<option <?php echo $state->state_id == $user->state ? 'selected' : '';?> value="<?php echo $state->state_id; ?>"> <?php echo $state->state_name; ?> </option>
																						<?php
																							}
																						}
																						?>
																					</select>
																				</div>
																			</div>
																			<div class="col-md-5">
																				<div class="form-group">
																					<label>City</label>
																					<select name="cty_name" id="showcity" class="form-control">
																						<option value=""> Select City </option>
																						<?php
																						$cities = $this->db->where('city_state_id',$user->state)->get('city')->result();
																						if (!empty($cities)){
																							foreach ($cities as $city) {
																						?>
																							<option <?php echo $city->city_id == $user->city ? 'selected' : '';?> value="<?php echo $city->city_id; ?>"> <?php echo $city->city_name; ?> </option>
																						<?php
																							}
																						}
																						?>
																					</select>
																				</div>
																			</div>
																		</div>
																	</div>

																	<div class="col-md-12">
																		<div class="row">
																			<div class="col-md-6">
																				<div class="form-group">
																					<label>Zip Code</label>
																					<input type="text" name="zip_code" class="form-control" maxlength="6" minlength="5" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" value="<?php echo $user->zipcode;?>" required />
																				</div>
																			</div>
																			<div class="col-md-5">
																				<div class="form-group">
																					<label>Phone Number</label>
																					<div class="row">
																						<div class="col-md-4">
																							<input type="text" name="phonecode" class="form-control" maxlength="4" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" value="<?php echo ltrim($user->phone_code,'+');?>" />
																						</div>
																						<div class="col-md-7">
																							<input type="text" name="ctc_number" class="form-control" maxlength="15" minlength="10" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" value="<?php echo $user->phone;?>" required />
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>

																	<div class="col-md-12">
																		<div class="row">
																			<div class="col-md-6">
																				<div class="form-group">
																					<label>Website</label>
																					<input type="url" name="web" class="form-control" placeholder="www.example.com" value="<?php echo $user->website;?>" />
																				</div>
																			</div>
																		</div>
																	</div>

																	<div class="col-md-12">
																		<div class="form-group">
																			<h4>Social Link</h4>
																		</div>
																		<hr/>
																		<div class="row">
																			<div class="col-md-6">
																				<div class="form-group">
																					<label>Facebook</label>
																					<input type="text" name="fburl" class="form-control" placeholder="www.facebook.com/John William" value="<?php echo $user->fb_url;?>" />
																				</div>
																			</div>
																			<div class="col-md-5">
																				<div class="form-group">
																					<label>Twitter</label>
																					<input type="text" name="twturl" class="form-control" placeholder="www.twitter.com/John William" value="<?php echo $user->twitter_url;?>" />
																				</div>
																			</div>
																		</div>
																	</div>

																	<div class="col-md-12">
																		<div class="row">
																			<div class="col-md-6">
																				<div class="form-group">
																					<label>LinkedIn</label>
																					<input type="text" name="lnkdinurl" class="form-control" placeholder="www.linkedIn.com/John William" value="<?php echo $user->lnkdin_url;?>" />
																				</div>
																			</div>
																		</div>
																	</div>

																	<div class="col-md-12">
																		<div class="form-group text-center">
																			<input type="submit" class="btn btn-primary" />
																		</div>
																	</div>
															</form>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- .container -->
	</div><!-- .page-content -->
</main><!-- .site-main -->
<script>
	function get_state(val) {
		$.ajax({url: "<?php echo base_url('home/stateAjax/');?>"+val, success: function(result){
			$("#showstate").html(result);
		}});
	}

	function get_city(val) {
		$.ajax({url: "<?php echo base_url('home/cityAjax/');?>"+val, success: function(result){
			$("#showcity").html(result);
		}});
	}
</script>
