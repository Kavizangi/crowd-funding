<?php
$company_announcements = $this->db->where('user_id',$this->session->userdata('eqty_userid'))->get('company_announcements')->result();
?>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>Financial Statements</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li>Financial Statements</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="account-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="account-content account-table">
						<h3 class="account-title">Financial Statements
							<button data-toggle="modal" data-target="#financialStatementsModal" class="btn btn-primary btn-table-sm pull-right">
								<i class="fa fa-plus"></i> Add
							</button>
							<br/>
							<br/>
						</h3>

						<?php
						if ( !empty($this->session->flashdata('crud_fd_success')) ){
							?>
							<div class="alert alert-success">
								<?php echo $this->session->flashdata('crud_fd_success');?>
							</div>
							<?php
						}else if ( !empty($this->session->flashdata('crud_fd_failed')) ){
							?>
							<div class="alert alert-danger">
								<?php echo $this->session->flashdata('crud_fd_failed');?>
							</div>
							<?php
						}
						?>

						<div class="account-main">
							<table class="table table-responsive dash_table">
								<thead>
									<tr>
										<th>#</th>
										<th>Title</th>
										<th>Remarks</th>
										<th>Document</th>
										<th>Review Comment</th>
										<th>Reviewed On</th>
										<th>Created On</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php $i=1;
									$statements = $this->db->where('user_id',$this->session->userdata('eqty_userid'))->get('financial_statements')->result();
									foreach ($statements as $statement) {
										$docs = json_decode($statement->document);
								?>
									<tr>
										<td><?php echo $i++;?></td>
										<td><?php echo $statement->title;?></td>
										<td><?php echo $statement->remarks;?></td>
										<td>
											<?php $i=1;
											foreach ($docs as $doc) {
											?>
												<p>
													<a href="<?php echo base_url($docs)?>" class="btn btn-link"><?php echo $i.'.';?> View</a>
												</p>
											<?php
												$i++;
											}
											?>
										</td>
										<td><?php echo $statement->review_comment;?></td>
										<td><?php echo $statement->reviewed_on;?></td>
										<td><?php echo $statement->created_on;?></td>
										<td>
											<button data-toggle="modal" data-target="#financialStatementsModal<?php echo $i;?>" class="btn btn-primary btn-table-sm">
												<i class="fa fa-eye">View &amp; Edit</i>
											</button>
											<a href="<?php echo base_url('user/delete/financial-statements/'.$statement->id)?>" onclick="return confirm('Are you sure to delete?');" class="btn btn-danger"><i class="fa fa-trash"></i> Delete </a>

											<div class="modal fade" id="financialStatementsModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="financialStatementsModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="modal-title" id="exampleModalLabel">Upload Financial Statement</h5>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<div class="row">
																<div class="col-md-12">
																	<form method="post" enctype="multipart/form-data" action="">

																		<div class="col-md-12">
																			<div class="field">
																				<label for="title">Title</label>
																				<input class="form-control" value="<?php echo $statement->title;?>" id="title" name="title" required/>
																			</div>
																		</div>

																		<div class="col-md-12">
																			<div class="field">
																				<label for="content">Remarks</label>
																				<textarea id="content" name="remarks"><?php echo $statement->remarks;?></textarea>
																			</div>
																		</div>

																		<div class="col-md-12">
																			<input type="file" name="file[]" class="form-control"/>
																			<br/>
																			<?php $i=1;
																			foreach ($docs as $doc) {
																				?>
																				<p>
																					<a href="<?php echo base_url($docs)?>" class="btn btn-link"><?php echo $i.'.';?> View</a>
																				</p>
																				<?php
																				$i++;
																			}
																			?>
																			<br/>
																		</div>

																		<div class="col-md-12">
																			<input type="hidden" name="action" value="edit"/>
																			<input type="hidden" name="id" value="<?php echo $statement->id;?>"/>
																			<button type="submit" name="save_action" class="btn-primary">Update Changes</button>
																		</div>
																	</form>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</td>
									</tr>
								<?php
									}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- .container -->
	</div><!-- .page-content -->

	<!-- Modal -->
	<div class="modal fade" id="financialStatementsModal" tabindex="-1" role="dialog" aria-labelledby="financialStatementsModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Upload Financial Statement</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<form method="post" enctype="multipart/form-data" action="">

									<div class="col-md-12">
										<div class="field">
											<label for="title">Title</label>
											<input class="form-control" id="title" name="title" required/>
										</div>
									</div>

									<div class="col-md-12">
										<div class="field">
											<label for="content">Remarks</label>
											<textarea id="content" name="remarks"></textarea>
										</div>
									</div>

									<div class="col-md-12">
										<p style="color: red">Upload a financial document</p>
										<input type="file" name="file[]" class="form-control" required/>
										<br/>
										<br/>
									</div>

									<div class="col-md-12">
										<button type="submit" name="save_action" class="btn-primary">Save Changes</button>
									</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main><!-- .site-main -->

<script>
	CKEDITOR.replace( 'content' );
</script>
