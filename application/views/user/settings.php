<?php
$user = $this->Auth_model->getLoggedInUserData();
?>
<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>Account Setting</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li>Account Setting</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="account-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<form method="post" class="row" action="">
									<div class="col-md-12">
										<h4>Account Settings</h4>
										<hr/>
										<?php
										if ( !empty($this->session->flashdata('setting_success')) ){
											?>
											<div class="alert alert-success">
												<?php echo $this->session->flashdata('setting_success');?>
											</div>
											<?php
										}else if ( !empty($this->session->flashdata('setting_failed')) ){
											?>
											<div class="alert alert-danger">
												<?php echo $this->session->flashdata('setting_failed');?>
											</div>
											<?php
										}
										?>
									</div>
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label>Account Holder</label>
													<input type="text" name="acct_name" value="<?php echo $user->acc_name;?>" class="form-control" required />
												</div>
											</div>

											<div class="col-md-5">
												<div class="form-group">
													<label>Account Number</label>
													<input type="text" name="acct_num" value="<?php echo $user->acc_num;?>"  class="form-control" required />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label>IFSC Code</label>
													<input type="text" name="ifsc_code" value="<?php echo $user->ifsc;?>" class="form-control" required />
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-12">
												<input type="submit" name="bank_det" class="btn btn-primary" value="Save Account">
											</div>
										</div>

									</div>
								</form>

								<br/>
								<br/>
								<br/>

								<form method="post" class="row" action="">
									<div class="col-md-12">
										<h4>Password Settings</h4>
										<hr/>
									</div>
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label>Current Password</label>
													<input type="password" name="old_pass" class="form-control" required />
												</div>
											</div>

											<div class="col-md-5">
												<div class="form-group">
													<label>New Password</label>
													<input type="password" name="new_pass" class="form-control" required />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label>Confirm Password</label>
													<input type="password" name="confirm_pass" class="form-control" required />
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-12">
												<input type="submit" name="account_password" class="btn btn-primary" value="Save Account">
											</div>
										</div>

									</div>
								</form>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- .container -->
	</div><!-- .page-content -->
</main><!-- .site-main -->
