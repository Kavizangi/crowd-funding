<?php
$user = $this->Auth_model->getLoggedInUserData();
?>
<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>Dashboard</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li>Dashboard</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="account-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="account-content dashboard">
						<h3 class="account-title">Dashboard</h3>
						<div class="account-main">
							<div class="author clearfix">
								<a class="author-avatar" href="#"><img src="<?php echo !empty($user->profile_image) ?  base_url("uploads/user-profile/".$user->profile_image) : base_url('assets/images/dashboard-avatar.png'); ?>" alt=""></a>
								<div class="author-content">
									<div class="author-title"><h3><a href="#"><?php echo $user->firstname." ".$user->lastname;?></a></h3><a class="edit-profile" href="<?php echo base_url('user/profile');?>">Edit Profile</a></div>
									<div class="author-info">
										<p><?php echo $user->email;?></p>
										<p>Member since <?php echo date('M Y',strtotime($user->crcdt));?></p>
									</div>
								</div>
							</div>
							<div class="dashboard-latest">
								<h3>My Latest Campaigns</h3>
								<ul>
									<?php
									$campaigns = $this->db->where('user_id',$this->session->userdata('eqty_userid'))->order_by('id','desc')->limit(5)->get('campaigns')->result();
									if (!empty($campaigns)) {
										foreach ( $campaigns as $campaign ) {
											$images = json_decode($campaign->images);
											$firstImage = $images[0];
											$author = $this->db->where('id', $campaign->user_id)->get('register')->row();
											$category = $this->db->where('id', $campaign->category)->get('category')->row();
									?>
											<li>
												<a href="<?php echo base_url('home/campaignDetails/'.$campaign->id); ?>">
													<img src="<?php echo base_url($firstImage);?>" alt="" class="col-md-5"></a>
												<div class="dashboard-latest-box">
													<div class="category"><a href="<?php echo base_url('home/campaignDetails/'.$campaign->id); ?>"><?php echo !empty($category->catagory_name) ? $category->catagory_name : ""?></a></div>
													<h4><a href="<?php echo base_url('home/campaignDetails/'.$campaign->id); ?>"><?php echo $campaign->title;?></a></h4>
												</div>
											</li>
									<?php
										}
									}
									?>

								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- .container -->
	</div><!-- .page-content -->
</main><!-- .site-main -->
