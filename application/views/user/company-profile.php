<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>Company Profile</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li>Company Profile</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<?php
		$companies_profile_data = $this->db->where('user_id',$this->session->userdata('eqty_userid'))->get('companies_profile')->row();
	?>
	<div class="account-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h4>Company Profile</h4>
					<hr/>
					<br/>
					<?php
					if ( !empty($this->session->flashdata('company_profile_success')) ){
					?>
						<div class="alert alert-success">
							<?php echo $this->session->flashdata('company_profile_success');?>
						</div>
					<?php
					}else if ( !empty($this->session->flashdata('company_profile_failed')) ){
					?>
						<div class="alert alert-danger">
							<?php echo $this->session->flashdata('company_profile_failed');?>
						</div>
					<?php
					}
					?>
					<form method="post" enctype="multipart/form-data" action="<?php echo base_url('user/companyProfile')?>">
						<div class="row">
							<div class="col-md-6">
								<div class="field">
									<label for="name_of_company">Name of company</label>
									<input type="text" id="name_of_company" name="name_of_company" value="<? echo @$companies_profile_data->name;?>" required/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="field">
									<label for="legal_name">Legal name</label>
									<input type="text" id="legal_name" name="legal_name" value="<? echo @$companies_profile_data->legal_name;?>" required/>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="field">
									<label for="business_status">Business status</label>
									<div class="field-select">
										<select name="business_status" required>
											<option <?php echo @$companies_profile_data->business_status == "public"? "selected" : "";?> value="public">Public</option>
											<option <?php echo @$companies_profile_data->business_status == "private"? "selected" : "";?> value="private">private</option>
											<option <?php echo @$companies_profile_data->business_status == "not-for-profit"? "selected" : "";?> value="not-for-profit">Not for profit</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="field">
									<label for="tin">Tax Identification Number (TIN)</label>
									<input type="text" id="tin" name="tin" value="<? echo @$companies_profile_data->tin;?>"/>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="field">
									<label for="address">Address </label>
									<input type="text" id="address" name="address" value="<? echo @$companies_profile_data->address;?>" required/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="field">
									<label for="city">City </label>
									<input type="text" id="city" name="city" value="<? echo @$companies_profile_data->city;?>" required/>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="field">
									<label for="country">Country </label>
									<input type="text" id="country" name="country" value="<? echo @$companies_profile_data->country;?>" required/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="field">
									<label for="commencement_date">Commencement date </label>
									<input type="text" id="commencement_date" class="datePicker" name="commencement_date" value="<? echo @$companies_profile_data->commencement_date;?>" required/>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="field">
									<label for="sector">Industry/Sector </label>
									<div class="field-select">
										<select name="sector" required>
											<option value="">--Select--</option>
											<option <?php echo @$companies_profile_data->sector == "Administration & Office Support"? "selected" : "";?> value="Administration & Office Support">Administration & Office Support</option>
											<option <?php echo @$companies_profile_data->sector == "Advertising, Arts & Media"? "selected" : "";?> value="Advertising, Arts & Media">Advertising, Arts & Media</option>
											<option <?php echo @$companies_profile_data->sector == "Banking & Financial Services"? "selected" : "";?> value="Banking & Financial Services">Banking & Financial Services</option>
											<option <?php echo @$companies_profile_data->sector == "Call Centre & Customer Service"? "selected" : "";?> value="Call Centre & Customer Service">Call Centre & Customer Service</option>
											<option <?php echo @$companies_profile_data->sector == "Community Services & Development"? "selected" : "";?> value="Community Services & Development">Community Services & Development</option>
											<option <?php echo @$companies_profile_data->sector == "Construction"? "selected" : "";?> value="Construction">Construction</option>
											<option <?php echo @$companies_profile_data->sector == "Consulting & Strategy"? "selected" : "";?> value="Consulting & Strategy">Consulting & Strategy</option>
											<option <?php echo @$companies_profile_data->sector == "Design & Architechture"? "selected" : "";?> value="Design & Architechture">Design & Architechture</option>
											<option <?php echo @$companies_profile_data->sector == "Education & Training"? "selected" : "";?> value="Education & Training">Education & Training</option>
											<option <?php echo @$companies_profile_data->sector == "Engineering"? "selected" : "";?> value="Engineering">Engineering</option>
											<option <?php echo @$companies_profile_data->sector == "Farming, Animals & Conservation"? "selected" : "";?> value="Farming, Animals & Conservation">Farming, Animals & Conservation</option>
											<option <?php echo @$companies_profile_data->sector == "Government & Defence"? "selected" : "";?> value="Government & Defence">Government & Defence</option>
											<option <?php echo @$companies_profile_data->sector == "Healthcare & Medical"? "selected" : "";?> value="Healthcare & Medical">Healthcare & Medical</option>
											<option <?php echo @$companies_profile_data->sector == "Hospitality & Tourism"? "selected" : "";?> value="Hospitality & Tourism">Hospitality & Tourism</option>
											<option <?php echo @$companies_profile_data->sector == "Human Resources & Recruitment"? "selected" : "";?> value="Human Resources & Recruitment">Human Resources & Recruitment</option>
											<option <?php echo @$companies_profile_data->sector == "Information & Communication Technology"? "selected" : "";?> value="Information & Communication Technology">Information & Communication Technology</option>
											<option <?php echo @$companies_profile_data->sector == "Insurance & Superannuation"? "selected" : "";?> value="Insurance & Superannuation">Insurance & Superannuation</option>
											<option <?php echo @$companies_profile_data->sector == "Legal"? "selected" : "";?> value="Legal">Legal</option>
											<option <?php echo @$companies_profile_data->sector == "Manufacturing, Transport & Logistics"? "selected" : "";?> value="Manufacturing, Transport & Logistics">Manufacturing, Transport & Logistics</option>
											<option <?php echo @$companies_profile_data->sector == "Marketing & Communications"? "selected" : "";?> value="Marketing & Communications">Marketing & Communications</option>
											<option <?php echo @$companies_profile_data->sector == "Mining, Resources & Energy"? "selected" : "";?> value="Mining, Resources & Energy">Mining, Resources & Energy</option>
											<option <?php echo @$companies_profile_data->sector == "Real Estate & Property"? "selected" : "";?> value="Real Estate & Property">Real Estate & Property</option>
											<option <?php echo @$companies_profile_data->sector == "Retail & Consumer Products"? "selected" : "";?> value="Retail & Consumer Products">Retail & Consumer Products</option>
											<option <?php echo @$companies_profile_data->sector == "Sales"? "selected" : "";?> value="Sales">Sales</option>
											<option <?php echo @$companies_profile_data->sector == "Science & Technology"? "selected" : "";?> value="Science & Technology">Science & Technology</option>
											<option <?php echo @$companies_profile_data->sector == "Sport & Recreation"? "selected" : "";?> value="Sport & Recreation">Sport & Recreation</option>
											<option <?php echo @$companies_profile_data->sector == "Trades & Services"? "selected" : "";?> value="Trades & Services">Trades & Services</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="field">
									<label for="stage">Stage </label>
									<div class="field-select">
										<select name="stage" required>
											<option <?php echo @$companies_profile_data->stage == "seed"? "selected" : "";?> value="seed">Seed</option>
											<option <?php echo @$companies_profile_data->stage == "startup"? "selected" : "";?> value="startup">Startup</option>
											<option <?php echo @$companies_profile_data->stage == "early"? "selected" : "";?> value="early">Early</option>
											<option <?php echo @$companies_profile_data->stage == "growth"? "selected" : "";?> value="growth">Growth</option>
											<option <?php echo @$companies_profile_data->stage == "expansion"? "selected" : "";?> value="expansion">Expansion</option>
											<option <?php echo @$companies_profile_data->stage == "matured"? "selected" : "";?> value="matured">Matured</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="field">
									<label for="email">Email </label>
									<input type="text" id="email" name="email" value="<? echo @$companies_profile_data->email;?>" required/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="field">
									<label for="telephone">Telephone </label>
									<input type="text" id="telephone" name="telephone" value="<? echo @$companies_profile_data->telephone;?>" required/>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="field">
									<label for="website">Website </label>
									<input type="text" id="website" name="website" value="<? echo @$companies_profile_data->bankers;?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="field">
									<label for="bankers">Bankers </label>
									<input type="text" id="bankers" name="bankers" value="<? echo @$companies_profile_data->bankers;?>"/>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="field">
									<label for="incorporation_date">Incorporation date </label>
									<input type="text" id="incorporation_date" class="datePicker" name="incorporation_date" value="<? echo @$companies_profile_data->incorporation_date;?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="field">
									<label for="about">About Company </label>
									<textarea id="about" rows="5" class="form-control" name="about" required><? echo @$companies_profile_data->about;?></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="field">
									<label for="mission">Mission Statement </label>
									<textarea id="mission" rows="5" class="form-control" name="mission"><? echo @$companies_profile_data->mission;?></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="field">
									<label for="vision">Vision Statement </label>
									<textarea id="vision" rows="5" class="form-control" name="vision"><? echo @$companies_profile_data->vision;?></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="field">
									<label for="upload">Upload (upload of supporting documents like incorporation certification, certificate to comment business, TIN certificate, less than 3 months utility bill/bank statement), </label>
									<input type="file" multiple id="upload" name="upload[]"/>
									<?
									if ( !empty($companies_profile_data->supporting_docs) ){
										foreach (json_decode($companies_profile_data->supporting_docs) as $doc) {
											?>
											<a href="<? echo $doc; ?>" target="_blank"
											   class="btn btn-link"><? echo str_replace("uploads/", "", $doc); ?></a><br/>
											<?
										}
									}
									?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="field">
									<label for="facebook">Facebook </label>
									<input type="text" id="facebook" name="facebook" value="<? echo @$companies_profile_data->facebook;?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="field">
									<label for="linkedin">Linkedin </label>
									<input type="text" id="linkedin" name="linkedin" value="<? echo @$companies_profile_data->linkedin;?>"/>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="field">
									<label for="google">Google </label>
									<input type="text" id="google" name="google" value="<? echo @$companies_profile_data->google;?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="field">
									<label for="twitter">Twitter </label>
									<input type="text" id="twitter" name="twitter" value="<? echo @$companies_profile_data->twitter;?>"/>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<button type="submit" name="save_action" value="save" class="btn-primary">Save Changes</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div><!-- .container -->
	</div><!-- .page-content -->
</main><!-- .site-main -->
