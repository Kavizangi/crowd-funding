<?php
$past_investors = $this->db->where('user_id',$this->session->userdata('eqty_userid'))->get('past_investors')->result();
?>
<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>Past Investors</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li>Past Investors</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="account-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="account-content account-table">
						<h3 class="account-title">Past Investors
							<button data-toggle="modal" data-target="#createPastInvestorsModal" class="btn btn-primary btn-table-sm pull-right">
								<i class="fa fa-plus"></i> Add
							</button>
							<br/>
							<br/>
						</h3>
						<?php
						if ( !empty($this->session->flashdata('crud_investor_success')) ){
							?>
							<div class="alert alert-success">
								<?php echo $this->session->flashdata('crud_investor_success');?>
							</div>
							<?php
						}else if ( !empty($this->session->flashdata('crud_investor_failed')) ){
							?>
							<div class="alert alert-danger">
								<?php echo $this->session->flashdata('crud_investor_failed');?>
							</div>
							<?php
						}
						?>
						<div class="account-main">
							<table class="table table-responsive dash_table">
								<thead>
									<tr>
										<th>#</th>
										<th>Name</th>
										<th>D.O.B</th>
										<th>TIN</th>
										<th>Job title</th>
										<th>No. of shares</th>
										<th>Value of shares</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php

								$i=1;
								if (!empty($past_investors)) {
									foreach ($past_investors as $datum) {
										$datum = (object) $datum;
										?>
										<tr>
											<td><? echo $i++;?></td>
											<td><? echo $datum->first_name.' '.$datum->surname?></td>
											<td><? echo $datum->dob;?></td>
											<td><? echo $datum->tin;?></td>
											<td><? echo $datum->job_title;?></td>
											<td><? echo $datum->no_of_shares;?></td>
											<td><? echo $datum->value_of_shares;?></td>
											<td>
												<button data-toggle="modal" data-target="#createPastInvestorsModal<?php echo $i;?>" class="btn btn-primary btn-table-sm">
													<i class="fa fa-eye">View &amp; Edit</i>
												</button>

												<a href="<?php echo base_url('user/delete/past-investor/'.$datum->id)?>" onclick="return confirm('Are you sure to delete?');" class="btn btn-danger"><i class="fa fa-trash"></i> Delete </a>

												<!-- Modal -->
												<div class="modal fade" id="createPastInvestorsModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="createPastInvestorsModalLabel" aria-hidden="true">
													<div class="modal-dialog modal-lg" role="document">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel">Create Past Investor</h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<div class="row">
																	<div class="col-md-12">
																		<form method="post" enctype="multipart/form-data" action="">
																			<input type="hidden" name="id" value="<?php echo $datum->id;?>">
																			<input type="hidden" name="action" value="edit">
																			<div class="row">
																				<div class="col-md-6">
																					<div class="field">
																						<label for="first_name">First name</label>
																						<input type="text" id="first_name" value="<?php echo $datum->first_name; ?>" name="first_name" required/>
																					</div>
																				</div>
																				<div class="col-md-6">
																					<div class="field">
																						<label for="surname">Surname</label>
																						<input type="text" id="surname" name="surname" value="<?php echo $datum->surname; ?>" required/>
																					</div>
																				</div>
																			</div>
																			<div class="row">
																				<div class="col-md-6">
																					<div class="field">
																						<label for="dob">D.O.B</label>
																						<input type="text" id="dob" name="dob" value="<?php echo $datum->dob; ?>" class="form-control datePicker"/>
																					</div>
																				</div>
																				<div class="col-md-6">
																					<div class="field">
																						<label for="resident_address">Resident Address </label>
																						<input type="text" id="resident_address" name="resident_address" value="<?php echo $datum->resident_address; ?>"/>
																					</div>
																				</div>
																			</div>
																			<div class="row">
																				<div class="col-md-6">
																					<div class="field">
																						<label for="resident_city">Resident City </label>
																						<input type="text" id="resident_city" name="resident_city" value="<?php echo $datum->resident_city; ?>"/>
																					</div>
																				</div>
																				<div class="col-md-6">
																					<div class="field">
																						<label for="resident_country">Resident Country </label>
																						<input type="text" id="resident_country" name="resident_country" value="<?php echo $datum->resident_country_citizenship; ?>"/>
																					</div>
																				</div>
																			</div>
																			<div class="row">
																				<div class="col-md-6">
																					<div class="field">
																						<label for="tin">TIN </label>
																						<input type="text" id="tin" name="tin" value="<?php echo $datum->tin; ?>"/>
																					</div>
																				</div>
																				<div class="col-md-6">
																					<div class="field">
																						<label for="investment_type">Investment Type </label>
																						<div class="field-select">
																							<select name="investment_type" required>
																								<option <?php echo $datum->investment_type=="Preference shares" ? "selected" : ""; ?> value="Preference shares">Preference shares</option>
																								<option <?php echo $datum->investment_type=="Ordinary shares" ? "selected" : ""; ?> value="Ordinary shares">Ordinary shares</option>
																								<option <?php echo $datum->investment_type=="Debt" ? "selected" : ""; ?> value="Debt">Debt</option>
																							</select>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="row">
																				<div class="col-md-6">
																					<div class="field">
																						<label for="no_shares">Number of shares </label>
																						<input type="text" id="no_of_shares" name="no_of_shares" value="<?php echo $datum->no_of_shares; ?>"/>
																					</div>
																				</div>
																				<div class="col-md-6">
																					<div class="field">
																						<label for="value_of_shares">Value of shares </label>
																						<input type="text" id="value_of_shares" name="value_of_shares" value="<?php echo $datum->value_of_shares; ?>"/>
																					</div>
																				</div>
																			</div>
																			<div class="row">
																				<div class="col-md-12">
																					<button type="submit" name="save_action" class="btn-primary">Save Changes</button>
																				</div>
																			</div>
																		</form>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

											</td>
										</tr>
										<?php
									}
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- .container -->
	</div><!-- .page-content -->

	<!-- Modal -->
	<div class="modal fade" id="createPastInvestorsModal" tabindex="-1" role="dialog" aria-labelledby="createPastInvestorsModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Create Past Investor</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<form method="post" enctype="multipart/form-data" action="">
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="first_name">First name</label>
											<input type="text" id="first_name" value="" name="first_name" required/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="field">
											<label for="surname">Surname</label>
											<input type="text" id="surname" name="surname" value="" required/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="dob">D.O.B</label>
											<input type="text" id="dob" name="dob" value="" class="form-control datePicker"/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="field">
											<label for="resident_address">Resident Address </label>
											<input type="text" id="resident_address" name="resident_address" value=""/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="resident_city">Resident City </label>
											<input type="text" id="resident_city" name="resident_city" value=""/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="field">
											<label for="resident_country">Resident Country </label>
											<input type="text" id="resident_country" name="resident_country" value=""/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="tin">TIN </label>
											<input type="text" id="tin" name="tin" value=""/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="field">
											<label for="investment_type">Investment Type </label>
											<div class="field-select">
												<select name="investment_type" required>
													<option value="Preference shares">Preference shares</option>
													<option value="Ordinary shares">Ordinary shares</option>
													<option value="Debt">Debt</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="no_shares">Number of shares </label>
											<input type="text" id="no_of_shares" name="no_of_shares" value=""/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="field">
											<label for="value_of_shares">Value of shares </label>
											<input type="text" id="value_of_shares" name="value_of_shares" value=""/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<button type="submit" name="save_action" class="btn-primary">Save Changes</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</main><!-- .site-main -->
