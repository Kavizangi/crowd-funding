<?php
	$directors = $this->db->where('user_id',$this->session->userdata('eqty_userid'))->get('directors')->result();
?>
<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>Directors</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li>Directors</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="account-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="account-content account-table">
						<h3 class="account-title">Directors
							<button data-toggle="modal" data-target="#createDirectorsModal" class="btn btn-primary btn-table-sm pull-right">
								<i class="fa fa-plus"></i> Add
							</button>
							<br/>
							<br/>
						</h3>

						<?php
						if ( !empty($this->session->flashdata('crud_director_success')) ){
						?>
							<div class="alert alert-success">
								<?php echo $this->session->flashdata('crud_director_success');?>
							</div>
						<?php
						}else if ( !empty($this->session->flashdata('crud_director_failed')) ){
						?>
							<div class="alert alert-danger">
								<?php echo $this->session->flashdata('crud_director_failed');?>
							</div>
						<?php
						}
						?>

						<div class="account-main">
							<table class="table table-responsive dash_table">
								<thead>
									<tr>
										<th>#</th>
										<th>Name</th>
										<th>D.O.B</th>
										<th>TIN</th>
										<th>Email</th>
										<th>Telephone</th>
										<th>Action</th>
									</tr>
								</thead>
									<tbody>
									<?php

									$i=1;
									if (!empty($directors)) {
										foreach ($directors as $datum) {
									?>
											<tr>
												<td><? echo $i++;?></td>
												<td><? echo $datum->title. ' '.$datum->first_name.' '.$datum->surname?></td>
												<td><? echo $datum->dob;?></td>
												<td><? echo $datum->tin;?></td>
												<td><? echo $datum->email;?></td>
												<td><? echo $datum->telephone;?></td>
												<td>
													<button data-toggle="modal" data-target="#createDirectorsModal<?php echo $i;?>" class="btn btn-primary btn-table-sm">
														<i class="fa fa-eye">View &amp; Edit</i>
													</button>

													<a href="<?php echo base_url('user/delete/directors/'.$datum->id)?>" onclick="return confirm('Are you sure to delete?');" class="btn btn-danger"><i class="fa fa-trash"></i> Delete </a>

													<!-- Modal -->
													<div class="modal fade" id="createDirectorsModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="createDirectorsModalLabel" aria-hidden="true">
														<div class="modal-dialog modal-lg" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="exampleModalLabel">Edit & View Director</h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true">&times;</span>
																	</button>
																</div>
																<div class="modal-body">
																	<div class="row">
																		<div class="col-md-12">
																			<form method="post" enctype="multipart/form-data" action="<?php echo base_url('user/companyDirectors')?>">
																				<input type="hidden" name="id" value="<?php echo $datum->id;?>">
																				<input type="hidden" name="action" value="edit">
																				<div class="row">
																					<div class="col-md-6">
																						<div class="field">
																							<label for="title">Title</label>
																							<input type="text" id="title" name="title" value="<?php echo $datum->title;?>" required/>
																						</div>
																					</div>
																					<div class="col-md-6">
																						<div class="field">
																							<label for="first_name">First name</label>
																							<input type="text" id="first_name" value="<?php echo $datum->first_name;?>" name="first_name" required/>
																						</div>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col-md-6">
																						<div class="field">
																							<label for="surname">Surname</label>
																							<input type="text" id="surname" name="surname" value="<?php echo $datum->surname;?>" required/>
																						</div>
																					</div>
																					<div class="col-md-6">
																						<div class="field">
																							<label for="dob">D.O.B</label>
																							<input type="text" id="dob" class="datePicker" name="dob" value="<?php echo $datum->dob;?>"/>
																						</div>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col-md-6">
																						<div class="field">
																							<label for="resident_address">Resident Address </label>
																							<input type="text" id="resident_address" name="resident_address" value="<?php echo $datum->resident_address;?>"/>
																						</div>
																					</div>
																					<div class="col-md-6">
																						<div class="field">
																							<label for="resident_city">Resident City </label>
																							<input type="text" id="resident_city" name="resident_city" value="<?php echo $datum->resident_city;?>"/>
																						</div>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col-md-6">
																						<div class="field">
																							<label for="resident_country">Resident Country </label>
																							<input type="text" id="resident_country" name="resident_country" value="<?php echo $datum->resident_country_citizenship;?>"/>
																						</div>
																					</div>
																					<div class="col-md-6">
																						<div class="field">
																							<label for="tin">TIN </label>
																							<input type="text" id="tin" name="tin" value="<?php echo $datum->tin;?>"/>
																						</div>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col-md-6">
																						<div class="field">
																							<label for="email">Email </label>
																							<input type="email" id="email" name="email"  value="<?php echo $datum->email;?>"  required/>
																						</div>
																					</div>
																					<div class="col-md-6">
																						<div class="field">
																							<label for="tel">Telephone </label>
																							<input type="text" id="tel" name="tel" value="<?php echo $datum->telephone;?>" required/>
																						</div>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col-md-6">
																						<div class="field">
																							<label for="highest_education">Highest Education </label>
																							<div class="field-select">
																								<select name="highest_education" required>
																									<option <?php echo $datum->highest_education == "Phd" ? "selected" : "";?> value="Phd">Phd</option>
																									<option <?php echo $datum->highest_education == "Master" ? "selected" : "";?> value="Master">Master</option>
																									<option <?php echo $datum->highest_education == "Bachelors" ? "selected" : "";?> value="Bachelors">Bachelors</option>
																									<option <?php echo $datum->highest_education == "High school" ? "selected" : "";?> value="High school">High school</option>
																								</select>
																							</div>
																						</div>
																					</div>
																					<div class="col-md-6">
																						<div class="field">
																							<label for="email">School </label>
																							<input type="text" id="school" value="<?php echo $datum->school;?>" name="school"/>
																						</div>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col-md-6">
																						<div class="field">
																							<label for="course_name">Course Name </label>
																							<input type="text" id="course_name" value="<?php echo $datum->course_name;?>" name="course_name"/>
																						</div>
																					</div>
																					<div class="col-md-6">
																						<div class="field">
																							<label for="completion_date">Completion Date </label>
																							<input type="text" class="datePicker" id="completion_date" value="<?php echo $datum->completion_date;?>" name="completion_date" />
																						</div>
																					</div>
																				</div>
																				<div class="col-md-12">
																					<br/>
																					<h4>Past Experience</h4>
																					<hr/>
																					<br/>
																				</div>
																				<div class="row">
																					<div class="col-md-6">
																						<div class="field">
																							<label for="past_organisation">Past Organisation </label>
																							<input type="text" id="past_organisation" value="<?php echo $datum->past_organisation;?>" name="past_organisation"/>
																						</div>
																					</div>
																					<div class="col-md-6">
																						<div class="field">
																							<label for="job_title">Job Title </label>
																							<input type="text" id="job_title" value="<?php echo $datum->job_title;?>" name="job_title"/>
																						</div>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col-md-6">
																						<div class="field">
																							<label for="start_date">Start Date </label>
																							<input type="text" id="start_date" value="<?php echo $datum->start_date;?>" class="form-control datePicker" name="start_date" />
																						</div>
																					</div>
																					<div class="col-md-6">
																						<div class="field">
																							<label for="end_date">End Date </label>
																							<input type="text" id="end_date" value="<?php echo $datum->end_date;?>" class="form-control datePicker" name="end_date" />
																						</div>
																					</div>
																				</div>
																				<div class="col-md-12">
																					<br/>
																					<h4>Social Media Links</h4>
																					<hr/>
																					<br/>
																				</div>
																				<div class="row">
																					<div class="col-md-6">
																						<div class="field">
																							<label for="linkedin">Linked In</label>
																							<input id="linkedin" class="form-control" value="<?php echo $datum->linkedin;?>" name="linkedin"/>
																						</div>
																					</div>

																					<div class="col-md-6">
																						<div class="field">
																							<label for="google">Google</label>
																							<input id="google" class="form-control" value="<?php echo $datum->google;?>" name="google"/>
																						</div>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col-md-6">
																						<div class="field">
																							<label for="twitter">Twitter</label>
																							<input id="twitter" class="form-control" value="<?php echo $datum->twitter;?>" name="twitter"/>
																						</div>
																					</div>
																				</div>

																				<div class="row">
																					<div class="col-md-12">
																						<button type="submit" name="save_action" class="btn-primary">Save Changes</button>
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>

									<?php
										}
									}
									?>
									</tbody>
								</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- .container -->
	</div><!-- .page-content -->

	<!-- Modal -->
	<div class="modal fade" id="createDirectorsModal" tabindex="-1" role="dialog" aria-labelledby="createDirectorsModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Create Director</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<form method="post" enctype="multipart/form-data" action="<?php echo base_url('user/companyDirectors')?>">
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="title">Title</label>
											<input type="text" id="title" name="title" value="" required/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="field">
											<label for="first_name">First name</label>
											<input type="text" id="first_name" value="" name="first_name" required/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="surname">Surname</label>
											<input type="text" id="surname" name="surname" value="" required/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="field">
											<label for="dob">D.O.B</label>
											<input type="text" class="datePicker" id="dob" name="dob" value=""/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="resident_address">Resident Address </label>
											<input type="text" id="resident_address" name="resident_address" value=""/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="field">
											<label for="resident_city">Resident City </label>
											<input type="text" id="resident_city" name="resident_city" value=""/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="resident_country">Resident Country </label>
											<input type="text" id="resident_country" name="resident_country" value=""/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="field">
											<label for="tin">TIN </label>
											<input type="text" id="tin" name="tin" value=""/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="email">Email </label>
											<input type="email" id="email" name="email"  value=""required/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="field">
											<label for="tel">Telephone </label>
											<input type="text" id="tel" name="tel" value="" required/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="highest_education">Highest Education </label>
											<div class="field-select">
												<select name="highest_education" required>
													<option value="Phd">Phd</option>
													<option value="Master">Master</option>
													<option value="Bachelors">Bachelors</option>
													<option value="High school">High school</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="field">
											<label for="email">School </label>
											<input type="text" id="school" value="" name="school"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="course_name">Course Name </label>
											<input type="text" id="course_name" value="" name="course_name"/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="field">
											<label for="completion_date">Completion Date </label>
											<input type="text" class="datePicker" id="completion_date" value="" name="completion_date" />
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<br/>
									<h4>Past Experience</h4>
									<hr/>
									<br/>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="past_organisation">Past Organisation </label>
											<input type="text" id="past_organisation" value="" name="past_organisation"/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="field">
											<label for="job_title">Job Title </label>
											<input type="text" id="job_title" value="" name="job_title"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="start_date">Start Date </label>
											<input type="text" id="start_date" value="" class="form-control datePicker" name="start_date" />
										</div>
									</div>
									<div class="col-md-6">
										<div class="field">
											<label for="end_date">End Date </label>
											<input type="text" id="end_date" value="" class="form-control datePicker" name="end_date" />
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<br/>
									<h4>Social Media Links</h4>
									<hr/>
									<br/>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="linkedin">Linked In</label>
											<input id="linkedin" class="form-control" value="" name="linkedin"/>
										</div>
									</div>

									<div class="col-md-6">
										<div class="field">
											<label for="google">Google</label>
											<input id="google" class="form-control" value="" name="google"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="field">
											<label for="twitter">Twitter</label>
											<input id="twitter" class="form-control" value="" name="twitter"/>
										</div>
									</div>
								</div>

								<div class="col-md-12">
									<button type="submit" name="save_action" class="btn-primary">Save Changes</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main><!-- .site-main -->
