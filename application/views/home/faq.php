<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>Faq</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/')?>">Home</a><span>/</span></li>
					<li>Faq</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="faq-content">
		<div class="container">
			<div class="faq-section">
				<div class="border-title">
					<h2 class="title left-title">FAQ</h2>
					<div class="description left-description">Frequently asked questions.</div>
				</div>
				<ul class="list-faq">
					<?php
						$faqs = $this->db->where('active_status',1)->get('faq')->result();
						foreach ($faqs as $faq) {
					?>
							<li>
								<span class="ion-plus"></span><a href="#"><?php echo $faq->question;?></a>
								<div class="faq-desc"><?php echo $faq->ans;?></div>
							</li>

					<?php
						}
					?>

				</ul>
			</div><!-- .faq-section -->
		</div>
	</div><!-- .faq-content -->
	<br/>
	<br/>
</main><!-- .site-main -->
