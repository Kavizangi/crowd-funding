<main id="main" class="site-main">

	<div class="page-title background-page">
		<div class="container">
			<h1>Who we are!</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li>About Us</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->

	<div class="container">

		<section class="statics section" style="padding-top: 0;">
			<h2 class="title">We are changing the way of making things possible.</h2>
			<div class="description">
				<p><?php echo $data['settings']['about_us'];?></p>
			</div>
		</section><!-- .statics -->
		<section class="team section">
			<h2 class="title">Meet Our Team</h2>
			<div class="description"><p>We are driven by a team of talented, experienced professionals.</p></div>
			<div class="team-content">
				<div class="row">
					<div class="col-lg-3 col-sm-6 col-6">
						<div class="team-item">
							<div class="avatar"><img src="<?php echo base_url('assets/images/support_01.jpg')?>" alt=""></div>
							<div class="team-info">
								<ul class="socials">
									<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
								</ul>
								<h3 class="team-name">Marie J. Smith</h3>
								<span class="team-job">Web Developer</span>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 col-6">
						<div class="team-item">
							<div class="avatar"><img src="<?php echo base_url('assets/images/support_02.jpg')?>" alt=""></div>
							<div class="team-info">
								<ul class="socials">
									<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
								</ul>
								<h3 class="team-name">Billy S. Tietjen</h3>
								<span class="team-job">Web Designer</span>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 col-6">
						<div class="team-item">
							<div class="avatar"><img src="<?php echo base_url('assets/images/support_03.jpg')?>" alt=""></div>
							<div class="team-info">
								<ul class="socials">
									<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
								</ul>
								<h3 class="team-name">Edward S. Agosto</h3>
								<span class="team-job">Digital Marketer</span>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 col-6">
						<div class="team-item">
							<div class="avatar"><img src="<?php echo base_url('assets/images/support_04.jpg')?>" alt=""></div>
							<div class="team-info">
								<ul class="socials">
									<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
								</ul>
								<h3 class="team-name">Clara C. Vinson</h3>
								<span class="team-job">Account Manager</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!-- .team -->

		<div class="partners partners-about">
			<div class="container">
				<div class="partners-slider owl-carousel">
					<div><a href="#"><img src="<?php echo base_url('assets/');?>images/partner-01.png" alt=""></a></div>
					<div><a href="#"><img src="<?php echo base_url('assets/');?>images/partner-02.png" alt=""></a></div>
					<div><a href="#"><img src="<?php echo base_url('assets/');?>images/partner-03.png" alt=""></a></div>
					<div><a href="#"><img src="<?php echo base_url('assets/');?>images/partner-04.png" alt=""></a></div>
					<div><a href="#"><img src="<?php echo base_url('assets/');?>images/partner-05.png" alt=""></a></div>
					<div><a href="#"><img src="<?php echo base_url('assets/');?>images/partner-06.png" alt=""></a></div>
				</div>
			</div>
		</div><!-- .partners -->
		<br/>
		<br/>
	</div><!-- .container -->
</main><!-- .site-main -->
