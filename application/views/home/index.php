<script>
	let sliderContent=new Array();
	let con = new Array();
</script>
<?php
	$campaigns = $this->db->where('status','live')->where('is_featured',1)->get('campaigns')->result();
	foreach ($campaigns as $campaign){
		$images = json_decode($campaign->images);
		$firstImage = $images[0];
		$created_on = strtotime($campaign->created_on);
		$end_date = strtotime($campaign->end_date);

		$date_diff = $end_date - $created_on;
		$days = round($date_diff / (60 * 60 * 24));

		$days_left = $days;

		$contributes = $this->db->where('prjt_id',$campaign->id)->where('pay_status',1)->get('contribute')->result();
		$funded=$this->db->select_sum('contribute_amount')->where('prjt_id',$campaign->id)->where('pay_status',1)->get('contribute')->row()->contribute_amount;
?>
	<script>
		con = Array();
		con['image'] = <?php echo json_encode(base_url($firstImage));?>;
		con['title'] = <?php echo json_encode(stripslashes($campaign->title));?>;
		con['desc'] = <?php echo json_encode(stripslashes($campaign->introduction));?>;
		con['button_link'] = <?php echo json_encode(base_url('home/campaignDetails/'.$campaign->id)); ?>;
		con['days_left'] = <?php echo json_encode($days_left)?>;
		con['funded'] = <?php echo json_encode($data['settings']['site_currency'].' '.number_format($funded));?>;
		con['target'] = <?php echo json_encode($data['settings']['site_currency'].' '.number_format($campaign->raise_amount))?>;
		con['percentage'] = <?php echo json_encode($funded/$campaign->raise_amount)?>;
		sliderContent.push(con);
	</script>
<?php
	}
?>
<style>
	.sideshow-content{
		display: none;
	}
</style>
<main id="main" class="site-main">
	<div class="sideshow" style="margin-left:5px;margin-right:5px;">
		<div class="container">
			<div class="sideshow-content">
				<h1 class="wow fadeInUp" data-wow-delay=".1s"></h1>
				<div class="sideshow-description wow fadeInUp" data-wow-delay=".1s"></div>
				<div class="process wow fadeInUp" data-scroll-nav="1">
					<div class="raised"><span style="width: 0%"></span></div>
					<div class="process-info">
						<div class="process-funded"><span></span>target</div>
						<div class="process-pledged"><span></span>funded</div>
						<div class="process-backers"><span></span>days left</div>
					</div>
				</div>
				<div class="button">
					<a href="#" target="_blank" class="btn-secondary">See Campaign</a>
				</div>
			</div><!-- .sideshow-content -->
		</div>
	</div><!-- .sideshow -->

	<?php $this->load->view('home/partials/home/loved-campaigns');?>

	<div class="how-it-work">
		<div class="container">
			<h2 class="title">See How It Work</h2>
			<div class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore.</div>
			<div class="row">
				<div class="col-lg-4">
					<div class="item-work">
						<div class="item-icon"><span>01</span><i class="fa fa-flask" aria-hidden="true"></i></div>
						<div class="item-content">
							<h3 class="item-title">Discover Ideas</h3>
							<div class="item-desc"><p>A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.</p></div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="item-work">
						<div class="item-icon"><span>02</span><i class="fa fa-leaf" aria-hidden="true"></i></div>
						<div class="item-content">
							<h3 class="item-title">Create a Campaigns</h3>
							<div class="item-desc"><p>Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues.</p></div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="item-work">
						<div class="item-icon"><span>03</span><i class="fa fa-money" aria-hidden="true"></i></div>
						<div class="item-content">
							<h3 class="item-title">Get Funded</h3>
							<div class="item-desc"><p>Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules.</p></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php $this->load->view('home/partials/home/discover-campaigns');?>

	<div class="partners">
		<div class="container">
			<div class="partners-slider owl-carousel">
				<div><a href="#"><img src="<?php echo base_url('assets/');?>images/partner-01.png" alt=""></a></div>
				<div><a href="#"><img src="<?php echo base_url('assets/');?>images/partner-02.png" alt=""></a></div>
				<div><a href="#"><img src="<?php echo base_url('assets/');?>images/partner-03.png" alt=""></a></div>
				<div><a href="#"><img src="<?php echo base_url('assets/');?>images/partner-04.png" alt=""></a></div>
				<div><a href="#"><img src="<?php echo base_url('assets/');?>images/partner-05.png" alt=""></a></div>
				<div><a href="#"><img src="<?php echo base_url('assets/');?>images/partner-06.png" alt=""></a></div>
			</div>
		</div>
	</div><!-- .partners -->
</main><!-- .site-main -->
<script>

	function doSlideShow(init){
		if(nextimage>=sliderContent.length){nextimage=0;}

		$('.sideshow')
			.css('background-image','url("'+sliderContent[nextimage]['image']+'")').css('background-position','cover!important')
			.fadeIn(4000,function(){
				let speed = 8000;
				if (init){
					speed = 1;
				}

				setTimeout(function () {

					$(".sideshow-content").css('display','block');

					$('.sideshow').find('h1').html(sliderContent[nextimage]['title']);
					$('.sideshow').find('.sideshow-description').html(sliderContent[nextimage]['desc']);
					$('.sideshow').find('.process-funded').find('span').html(sliderContent[nextimage]['target']);
					$('.sideshow').find('.process-pledged').find('span').html(sliderContent[nextimage]['funded']);
					$('.sideshow').find('.process-backers').find('span').html(sliderContent[nextimage]['days_left']);
					$('.sideshow').find('.raised').find('span').css('width',(parseFloat(sliderContent[nextimage]['percentage']))+"%");
					$('.sideshow').find('a').attr('href',sliderContent[nextimage]['button_link']);
					nextimage++;
					doSlideShow(false);
				},speed);
			});
	}

	let nextimage=0;
	doSlideShow(true);
</script>
