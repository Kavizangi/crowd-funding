<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>404</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li>404</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="container">
		<div class="main-content main-404">
			<img src="<?php echo base_url('images/404.png');?>" alt="">
			<h2>Page not found...</h2>
			<div class="button">
				<a href="<?php echo base_url('/')?>" class="btn-secondary">Back to home</a>
				<a href="" class="btn-primary">Explore all Campaigns</a>
			</div>
		</div>
	</div><!-- .container -->
</main><!-- .site-main -->
