<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/payments.css');?>" />
<style>
	.inputGroup table td{
		font-weight: normal!important;
	}
</style>
<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>Thank You!</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li>Make Investment</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="page-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<form class="form" method="post" action="<?php echo base_url('home/doPayment')?>">
						<h2>Payment Method</h2>
						<br/>
						<div class="inputGroup">
							<input id="radio1" name="radio" type="radio" value="paypal" />
							<label for="radio1"><img src="<?php echo base_url('assets/images/paypal.png')?>" style="width: 150px;"/></label>
						</div>
						<div class="inputGroup">
							<input id="radio2" name="radio" type="radio" value="paystack" />
							<label for="radio2"><img src="<?php echo base_url('assets/images/paystack.jpeg')?>" style="width: 150px;"/></label>
						</div>
						<div class="inputGroup">
							<input id="radio3" name="radio" type="radio" value="bank" />
							<label for="radio3">Offline Pay</label>
							<?php
							$ad_bank_details = $this->db->get('ad_bank_details')->row();
							?>
							<table class="table table-condensed">
								<tr>
									<td> Name of the Account holder</td>
									<td><?php echo $ad_bank_details->acct_name; ?></td>
								</tr>
								<tr>
									<td> Name of the Bank</td>
									<td> <?php echo $ad_bank_details->bank_name; ?></td>
								</tr>
								<tr>
									<td> Name of the Branch Code</td>
									<td><?php echo $ad_bank_details->branch_name; ?></td>
								</tr>
								<tr>
									<td> Account Number</td>
									<td> <?php echo $ad_bank_details->account_num; ?></td>
								</tr>
								<tr>
									<td>SWIFT Code</td>
									<td> <?php echo $ad_bank_details->ifsc; ?></td>
								</tr>
							</table>
						</div>
						<br/>
						<input type="hidden" value="<?php echo $data['amount'];?>" name="amount" />
						<input type="hidden" name="campaign_id" value="<?php echo $data['campaign_id'];?>"/>

						<button type="submit" class="btn btn-success">Next Step</button>
					</form>
				</div>
			</div>
		</div><!-- .container -->
	</div><!-- .page-content -->
</main><!-- .site-main -->
