<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>Say Hello!</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li>Contact Us</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="page-content contact-content">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 main-content">
					<div class="entry-content">
						<div class="row">
							<div class="col-lg-8">
								<div class="form-contact">
									<h2>Drop US a line</h2>
									<?php
										if (!empty($this->session->flashdata('success'))){
									?>
										<div class="alert alert-success"><?php echo $this->session->flashdata('success');?></div>
									<?php
										}else if (!empty($this->session->flashdata('failed'))){
									?>
											<div class="alert alert-danger"><?php echo $this->session->flashdata('failed');?></div>
									<?php
										}
									?>
									<form action="<?php echo base_url('home/contact');?>" method="POST" id="contactForm" class="clearfix">
										<div class="clearfix">
											<div class="field align-left">
												<input type="text" value="" name="name" placeholder="Your Name" required/>
											</div>
											<div class="field align-right">
												<input type="text" value="" name="email" placeholder="Your Email" required/>
											</div>
										</div>
										<div class="field">
											<input type="text" value="" name="subject" placeholder="Subject" required/>
										</div>
										<div class="field-textarea">
											<textarea rows="8" placeholder="Message" name="message" required></textarea>
										</div>
										<button type="submit" name="submit" class="btn-primary">Submit Message</button>
									</form>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="contact-info">
									<h3>Contact Information</h3>
									<ul>
										<li><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo stripslashes($data['settings']['admin_address']);?></li>
										<li><i class="fa fa-phone" aria-hidden="true"></i><?php echo $data['settings']['site_number'];?></li>
										<li><i class="fa fa-envelope-o" aria-hidden="true"></i><?php echo $data['settings']['site_email'];?></li>
									</ul>
									<div class="contact-desc"><p></p></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- .container -->
	</div><!-- .page-content -->
	<div class="maps">
		<div id="map"></div>
	</div><!-- .maps -->
</main><!-- .site-main -->
