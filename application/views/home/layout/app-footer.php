<footer id="footer" class="site-footer">
	<div class="footer-menu">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-sm-4 col-4">
					<div class="footer-menu-item">
						<h3>Our company</h3>
						<ul>
							<li><a href="<?php echo base_url('/')?>">Home</a></li>
							<li><a href="<?php echo base_url('home/about');?>">About</a></li>
							<li><a href="<?php echo base_url('home/faq');?>">FAQ</a></li>
							<li><a href="<?php echo base_url('home/exploreCampaign');?>">Explore</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-sm-4 col-4">
					<div class="footer-menu-item">
						<h3>Campaign</h3>
						<ul>
							<li><a href="<?php echo base_url('home/startCampaign');?>">Start Your Campaign</a></li>
							<li><a href="<?php echo base_url('home/privacyPolicy');?>">Privacy Policy</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-sm-4 col-4">
					<div class="footer-menu-item">
						<h3>Explore</h3>
						<ul>
							<?php
								$categories = $this->db->get("category")->result();
								foreach ($categories as $category) {
							?>
									<li><a href="<?php echo base_url('home/exploreCampaign/'.$category->id);?>"><?php echo $category->catagory_name;?></a></li>
							<?php
								}
							?>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-sm-12 col-12">
					<div class="footer-menu-item newsletter">
						<div class="follow">
							<h3>Follow us</h3>
							<ul>
								<li class="facebook"><a target="_Blank" href="<?php echo $data['settings']['facebook'];?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li class="twitter"><a target="_Blank" href="<?php echo $data['settings']['twitter'];?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li class="instagram"><a target="_Blank" href="<?php echo $data['settings']['linkedin'];?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
								<li class="google"><a target="_Blank" href="<?php echo $data['settings']['gplus'];?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- .footer-menu -->
	<div class="footer-copyright">
		<div class="container">
			<p class="copyright"><?php echo date('Y').' By '.$data['settings']['site_title'];?>. All Rights Reserved.</p>
			<a href="#" class="back-top">Back to top<span class="ion-android-arrow-up"></span></a>
		</div>
	</div>
</footer><!-- site-footer -->
</div><!-- #wrapper -->
<script src="<?php echo base_url('assets/libs/popper/popper.js');?>"></script>
<script src="<?php echo base_url('assets/libs/bootstrap/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/libs/owl-carousel/owl.carousel.min.js');?>"></script>
<script src="<?php echo base_url('assets/libs/owl-carousel/carousel.min.js');?>"></script>
<script src="<?php echo base_url('assets/libs/jquery.countdown/jquery.countdown.min.js');?>"></script>
<script src="<?php echo base_url('assets/libs/wow/wow.min.js');?>"></script>
<script src="<?php echo base_url('assets/libs/isotope/isotope.pkgd.min.js');?>"></script>
<script src="<?php echo base_url('assets/libs/bxslider/jquery.bxslider.min.js');?>"></script>
<script src="<?php echo base_url('assets/libs/magicsuggest/magicsuggest-min.js');?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.6/tinymce.min.js" type="text/javascript"></script>
<script>tinymce.init({ selector:'.tinymce' });</script>
<!-- other script -->
<script src="<?php echo base_url('assets/js/main.js');?>"></script>

<script>
	$( ".datePicker" ).datepicker({
		dateFormat: 'yy-mm-dd'
	});

	function validate(evt) {
		let theEvent = evt || window.event;
		let key = theEvent.keyCode || theEvent.which;
		key = String.fromCharCode( key );
		let regex = /[0-9]|\./;
		if( !regex.test(key) ) {
			theEvent.returnValue = false;
			if(theEvent.preventDefault) theEvent.preventDefault();
		}
	}
</script>
<?php
if ($data['page_name']=="contact"){
?>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLLMir36iGtKwpbt_mR7H2zBYlgkupzW0&callback=initGoogleMap" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/contact.js');?>"></script>
<?php
}
?>
</body>
</html>
