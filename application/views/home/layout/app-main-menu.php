<div class="right-header">
	<nav class="main-menu">
		<button class="c-hamburger c-hamburger--htx"><span></span></button>
		<ul>
			<li><a href="<?php echo base_url('/')?>">Home</a></li>
			<li><a href="<?php echo base_url('home/exploreCampaign');?>">Explore</a></li>
			<li><a href="<?php echo base_url('home/companies');?>">Companies</a></li>
			<li><a href="<?php echo base_url('home/startCampaign');?>">Start Fund Raising</a></li>
			<li><a href="<?php echo base_url('home/about');?>">About</a></li>
			<li><a href="<?php echo base_url('home/faq');?>">FAQ</a></li>
			<li><a href="<?php echo base_url('home/contact');?>">Contact</a></li>
			<?php
				if ($this->Auth_model->is_loggedIn()) {
			?>
					<li>
						<a href="#">Account<i class="fa fa-caret-down" aria-hidden="true"></i></a>
						<ul class="sub-menu">
							<li><a href="<?php echo base_url('user');?>">Dashboard</a></li>
							<li><a href="<?php echo base_url('user/profile');?>">Account Profile</a></li>
							<li><a href="<?php echo base_url('user/companyProfile');?>">Company Profile</a></li>
							<li><a href="<?php echo base_url('user/companyDirectors');?>">Directors</a></li>
							<li><a href="<?php echo base_url('user/companyManagement');?>">Management</a></li>
							<li><a href="<?php echo base_url('user/pastInvestors');?>">Past Investors</a></li>
							<li><a href="<?php echo base_url('user/campaign');?>">My Campaigns</a></li>
							<li><a href="<?php echo base_url('user/announcements');?>">Announcements</a></li>
							<li><a href="<?php echo base_url('user/payments');?>">Backers</a></li>
							<li><a href="<?php echo base_url('user/myInvestments');?>">My Investments</a></li>
							<li><a href="<?php echo base_url('user/campaignsIFollow');?>">Campaigns I follow</a></li>
							<li><a href="<?php echo base_url('user/settings');?>">Settings</a></li>
							<li><a href="<?php echo base_url('user/announcements');?>">Announcements</a></li>
							<li><a href="<?php echo base_url('user/financialStatements');?>">Financial statements</a></li>
						</ul>
					</li>
			<?php
				}
			?>
		</ul>
	</nav><!-- .main-menu -->

	<?php
		if ($this->Auth_model->is_loggedIn()) {
	?>
			<div class="login login-button">
				<a href="<?php echo base_url('auth/logout'); ?>" class="btn-primary">Logout</a>
			</div><!-- .login -->
	<?php
		}else {
	?>

			<div class="login login-button">
				<a href="<?php echo base_url('auth/login'); ?>" class="btn-primary">Login</a>
			</div><!-- .login -->

	<?php
		}
	?>
</div><!--. right-header -->
