<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />

<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $data['page_title'];?></title>

	<!-- Style CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style.css');?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/responsive.css');?>" />
	<!--datepicker-->
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/smoothness/jquery-ui.css"/>
	<link rel="icon" href="<?php echo base_url("/uploads/settings/".$data['settings']['site_icon']); ?>" type="image/x-icon"/>
	<!-- jQuery -->
	<script src="<?php echo base_url('assets/js/jquery-1.12.4.min.js'); ?>"></script>
	<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
	<style>
		.table > tbody > tr > td,
		.table > tbody > tr > th,
		.table > tfoot > tr > td,
		.table > tfoot > tr > th,
		.table > thead > tr > td,
		.table > thead > tr > th {
			border-color: #ccc!important;
		}

		.btn-table-sm{
			padding: 5px 10px!important;
			height: auto!important;
			line-height: unset!important;
		}

		#ui-datepicker-div{
			z-index:9999!important;
		}

		.campaign-content .campaign-box .process .process-info span{
			margin-right: 35px!important;
		}

		.campaign-item > a img{
			max-height: 300px!important;
			min-height: 300px!important;
		}
</style>
</head>

<body class="<?php echo $this->uri->segment(2) == 'campaignDetails' ? 'campaign-detail' : ($this->uri->segment(1)=='user' || $this->uri->segment(2)=='exploreCampaign' ? '' : 'home home-top');?>">
<!--<div class="preloading">-->
<!--	<div class="preloader loading">-->
<!--		<span class="slice"></span>-->
<!--		<span class="slice"></span>-->
<!--		<span class="slice"></span>-->
<!--		<span class="slice"></span>-->
<!--		<span class="slice"></span>-->
<!--		<span class="slice"></span>-->
<!--	</div>-->
<!--</div>-->
<div id="wrapper">
	<header id="header" class="site-header">
		<div class="top-header clearfix">
			<div class="container">
				<ul class="socials-top">
					<li><a target="_Blank" href="<?php echo $data['settings']['facebook'];?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a target="_Blank" href="<?php echo $data['settings']['twitter'];?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a target="_Blank" href="<?php echo $data['settings']['gplus'];?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					<li><a target="_Blank" href="<?php echo $data['settings']['linkedin'];?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
				</ul>
				<div class="phone">Call Now <?php echo $data['settings']['site_number'];?></div>
			</div>
		</div>
		<div class="content-header">
			<div class="container">
				<div class="site-brand">
					<a href="<?php echo base_url('/')?>"><img src="<?php echo base_url("/uploads/settings/".$data['settings']['site_logo']); ?>" alt="Logo"></a>
				</div><!-- .site-brand -->
				<?php
					include 'app-main-menu.php';
				?>
			</div><!-- .container -->
		</div>
	</header><!-- .site-header -->
