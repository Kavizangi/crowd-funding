<style>
	.campaign-history-tabs{
		display: none;
	}
	.campaign-history-tabs.active{
		display: block;
	}
</style>
<script>
	$(document).on("click",".campaign-history-navs",function () {
		$(".campaign-history-navs").removeClass("active");
		$(this).addClass("active");

		$(".campaign-history-tabs").hide();
		$('#'+ $(this).data('tab') ).show();

	});
</script>

<div class="campaign-history">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h2 class="title">Discover Campaigns</h2>
				<br/>
				<div class="campaign-tabs text-center">
					<ul class="tabs-controls">
						<?php
						$i=0;
						$categories = $this->db->query("select category.* from category RIGHT JOIN campaigns ON campaigns.category = category.id LIMIT 5")->result();
						$highest_categories = $this->db->query("SELECT category as id,COUNT(*) as total_category FROM campaigns GROUP BY category ORDER BY total_category DESC")->row();

						foreach ($categories as $category) {
							$this_filter = "filter".$i;
							?>
							<li class="campaign-history-navs <?php echo $highest_categories->id == $category->id ? 'active' : '';?>" data-tab="<?php echo $this_filter;?>"><a href="#"><?php echo $category->catagory_name;?></a></li>
							<?php
							$i++;
						}
						?>
					</ul>
				</div>
				<div class="campaign-content">
					<?php
						$i=0;
						foreach ($categories as $category) {
							$this_filter = "filter" . $i;
					?>
							<div id="<?php echo $this_filter;?>" class="campaign-history-tabs tabs <?php echo trim($highest_categories->id) == trim($category->id) ? 'active' : '';?>">
								<?php
									$this_campaigns = $this->db->where('category',$category->id)->where('status','live')->limit(10)->get('campaigns')->result();
									if (!empty($this_campaigns)){
										foreach ($this_campaigns as $this_campaign) {

											$images = json_decode($this_campaign->images);
											$firstImage = $images[0];
											$author = $this->db->where('id',$this_campaign->user_id)->get('register')->row();
											$category = $this->db->where('id',$this_campaign->category)->get('category')->row();
											$funded=$this->db->select_sum('contribute_amount')->where('prjt_id',$this_campaign->id)->where('pay_status',1)->get('contribute')->row()->contribute_amount;

								?>
										<div class="col-lg-4 col-md-6 col-sm-6 col-6">
											<div class="campaign-item">
												<a class="overlay" href="<?php echo base_url('home/campaignDetails/'.$this_campaign->id); ?>">
													<img src="<?php echo base_url($firstImage);?>" alt="">
													<span class="ion-ios-search-strong"></span>
												</a>
												<div class="campaign-box">
													<a href="<?php echo base_url('home/exploreCampaign/'.$this_campaign->id); ?>" class="category"><?php echo !empty($category->catagory_name) ? $category->catagory_name : ""?></a>
													<h3><a href="<?php echo base_url('home/campaignDetails/'.$this_campaign->id); ?>">
															<?php echo $this_campaign->title;?></a></h3>
													<div class="campaign-description">
														<?php
															echo $this_campaign->introduction;
														?>
													</div>
													<div class="campaign-author">
														<a class="author-icon" href="#"><img src="<?php echo !empty($author->profile_image) ?  base_url("uploads/user-profile/".$author->profile_image) : base_url('assets/images/dashboard-avatar.png'); ?>" alt=""></a>by <a
															class="author-name" href="#"><?php echo $author->firstname.' '.$author->lastname;?></a></div>
													<div class="process">
														<div class="raised"><span style="width: <?php echo ($funded/$this_campaign->raise_amount)?>%"></span></div>
														<div class="process-info">
															<div class="process-pledged">
																<span>
																	<?php echo $data['settings']['site_currency'].' '.number_format($this_campaign->raise_amount);?>
																</span>
																Target
															</div>
															<div class="process-funded"><span><?php echo $data['settings']['site_currency'].' '.number_format($funded);?></span>Funded</div>
															<div class="process-time"><span>
																	<?php
																		$created_on = strtotime($this_campaign->created_on);
																		$end_date = strtotime($this_campaign->end_date);

																		$date_diff = $end_date - $created_on;
																		$days = round($date_diff / (60 * 60 * 24));

																		echo $days;
																	?>
																</span>Days left</div>
														</div>
													</div>
												</div>
											</div>
										</div>
								<?php
										}
									}
								?>
							</div>
					<?php
							$i++;
						}
					?>
				</div>
				<br/>
				<div class="latest-button"><a href="<?php echo base_url('home/exploreCampaign');?>" class="btn-primary">View all Campaigns</a></div>
				<br/>
				<br/>
				<br/>
			</div>
		</div>
	</div>
</div><!-- .latest -->
