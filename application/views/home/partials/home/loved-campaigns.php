<div class="project-love">
	<div class="container">
		<h2 class="title">Projects We Love</h2>
		<div class="description">See the list of projects we would love you support.</div>
		<div class="tab-menu tab-row">
			<ul id="bx-pager" class="menu-category">
				<?php $i=0;
				$categories = $this->db->query("select category.* from category RIGHT JOIN campaigns ON campaigns.category = category.id")->result();
				foreach ($categories as $category) {
					?>
					<li class="mc-option <?php echo $i==0 ? 'active' : '';?>" data-tab="pp<?php echo $i;?>">
						<a href="#" data-slide-index="<?php echo $i;?>"><?php echo $category->catagory_name;?></a>
					</li>
					<?php
					$i++;
				}
				?>
				<li class="cat-more"><a href="<?php echo base_url('home/exploreCampaign');?>">More</a></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="popular-project">
					<?php $i=0;
					foreach ($categories as $category) {
						$this_campaign = $this->db->where('category',$category->id)->where('status','live')->get('campaigns')->result();
						?>
						<div id="pp<?php echo $i;?>" class="pp-item <?php echo $i==0 ? 'active' : '';?>">
							<i class="fa fa-file"></i>
							<h4><?php echo count($this_campaign) > 0 ? count($this_campaign)."+ " : "";?> Project <?php echo $category->catagory_name;?></h4>
						</div>
						<?php
						$i++;
					}
					?>
					<p>Discover projects just for you and get great recommendations when you select your interests.</p>
					<a href="<?php echo base_url('home/startCampaign');?>" class="btn-primary">Start a Campaign</a>
				</div>
			</div>
			<div class="col-lg-8">
				<ul class="project-love-slider pls-col">
					<?php $i=0;
					foreach ($categories as $category) {
						$this_campaign = $this->db->where('category', $category->id)->where('status', 'live')->order_by('id', 'RANDOM')->get('campaigns')->result();
						if (!empty($this_campaign[0])) {
							$this_campaign = $this_campaign[0];
							$images = json_decode($this_campaign->images);
							$firstImage = $images[0];
							$author = $this->db->where('id', $this_campaign->user_id)->get('register')->row();
							$funded=$this->db->select_sum('contribute_amount')->where('prjt_id',$this_campaign->id)->where('pay_status',1)->get('contribute')->row()->contribute_amount;
							$country = $this->db->where('country_id',$this_campaign->country)->get('country')->row();
					?>
							<li>
								<div class="project-love-item clearfix">
									<a class="project-love-image"
									   href="<?php echo base_url('home/campaignDetails/' . $this_campaign->id); ?>">
										<img src="<?php echo base_url($firstImage); ?>" alt="">
									</a>
									<div class="project-love-item-content project-love-box">
										<a href="<?php echo base_url('home/exploreCampaign/' . $category->id); ?>" class="category"><?php echo $category->catagory_name; ?></a>
										<h3>
											<a href="<?php echo base_url('home/campaignDetails/' . $this_campaign->id); ?>"><?php echo $this_campaign->title; ?></a>
										</h3>
										<div class="project-love-description">
											<?php
												echo $this_campaign->introduction;
											?>
										</div>
										<div class="project-love-author">
											<div class="author-profile">
												<a class="author-avatar" href="#">
													<img
														src="<?php echo !empty($author->profile_image) ? base_url("uploads/user-profile/" . $author->profile_image) : base_url('assets/images/dashboard-avatar.png'); ?>"
														alt=""></a>by
												<a class="author-name"
												   href="#"><?php echo $author->firstname . ' ' . $author->lastname; ?></a>
											</div>
											<div class="author-address"><span class="ion-location"></span><?php echo $this_campaign->city.' '.$country->country_name;?></div>
										</div>
										<div class="process">
											<div class="raised"><span style="width: <?php echo $funded/$this_campaign->raise_amount;?>%"></span></div>
											<div class="process-info">
												<div class="process-pledged">
													<span>
														<?php echo $data['settings']['site_currency'] . ' ' . number_format($this_campaign->raise_amount); ?>
													</span>
													Target
												</div>
												<div class="process-funded">
													<span><?php echo $data['settings']['site_currency'] . ' '.number_format($funded); ?></span>Funded
												</div>
												<div class="process-time"><span>
													<?php
														$created_on = strtotime($this_campaign->created_on);
														$end_date = strtotime($this_campaign->end_date);

														$date_diff = $end_date - $created_on;
														$days = round($date_diff / (60 * 60 * 24));

														echo $days;
													?>
													</span>Days left
												</div>
											</div>
										</div>
									</div>
								</div>
							</li>
							<?php
						}else{
							?>
							<li>
								<div class="project-love-item clearfix"></div>
							</li>
							<?php
						}
						$i++;
					}
					?>
				</ul>
			</div>
		</div>
	</div>
</div><!-- .project-love -->
