<main id="main" class="site-main">
	<div class="page-title background-campaign">
		<div class="container">
			<h1>Start a campaign</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/')?>">Home</a><span>/</span></li>
					<li>Start a campaign</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="campaign-form form-update">
		<div class="container">
			<form action="#" method="post" enctype="multipart/form-data">
				<h4>Start a campaign</h4>
				<?php
				if ( !empty($this->session->flashdata('create_campaign_success')) ){
				?>
					<div class="alert alert-success">
						<?php echo $this->session->flashdata('create_campaign_success');?>
					</div>
				<?php
				}else if ( !empty($this->session->flashdata('create_campaign_failed')) ){
				?>
					<div class="alert alert-danger">
						<?php echo $this->session->flashdata('create_campaign_failed');?>
					</div>
				<?php
				}
				?>
				<div class="field">
					<label for="title">Campaign Title *</label>
					<span class="label-desc">What is the title of your campaign?</span>
					<input type="text" value="<?php echo set_value('title');?>" id="title" name="title" placeholder="" required>
				</div>
				<div class="field">
					<label for="introduction">Introduction *</label>
					<span class="label-desc">Provide a short introduction that best describes your campaign to your audience.</span>
					<textarea rows="4" class="tinymce" id="introduction" name="introduction" placeholder="Write here.."><?php echo set_value('introduction');?></textarea>
				</div>
				<div class="field">
					<label for="problem">The Problem *</label>
					<span class="label-desc">Introduce the problem associated with the campaign.</span>
					<textarea rows="4" class="tinymce" id="problem" name="problem" placeholder="Write here.."><?php echo set_value('problem');?></textarea>
				</div>
				<div class="field">
					<label for="solution">The Solution *</label>
					<span class="label-desc">Describe the solution with this campaign.</span>
					<textarea rows="4" class="tinymce" id="solution" name="solution" placeholder="Write here.."><?php echo set_value('solution');?></textarea>
				</div>
				<div class="field">
					<label for="intended_results">Intended Results *</label>
					<span class="label-desc">Describe the intended results with this campaign.</span>
					<textarea rows="4" class="tinymce" id="intended_results" name="intended_results" placeholder="Write here.."><?php echo set_value('intended_results');?></textarea>
				</div>
				<div class="field">
					<label for="accomplishment_so_far">Accomplishment So far *</label>
					<span class="label-desc">What have you been able to accomplish.</span>
					<textarea rows="4" class="tinymce" id="accomplishment_so_far" name="accomplishment_so_far" placeholder="Write here.."><?php echo set_value('accomplishment_so_far');?></textarea>
				</div>
				<div class="field">
					<label for="solution">Revenue Strategy *</label>
					<span class="label-desc">Describe your revenue strategy.</span>
					<textarea rows="4" class="tinymce" id="revenue_strategy" name="revenue_strategy" placeholder="Write here.." ><?php echo set_value('revenue_strategy');?></textarea>
				</div>
				<div class="field">
					<label for="use_of_funds">Use of Funds *</label>
					<span class="label-desc">Describe the use funds.</span>
					<textarea rows="4" class="tinymce" id="use_of_funds" name="use_of_funds" placeholder="Write here.."><?php echo set_value('use_of_funds');?></textarea>
				</div>
				<div class="field">
					<label for="investment_terms">Investment Terms *</label>
					<div class="field-select">
						<select name="investment_terms" id="investment_terms" required>
							<option value="">Select investment terms</option>
							<option value="Loan">Loan</option>
							<option value="Preference Shares">Preference Shares</option>
							<option value="Common Shares">Common Shares</option>
						</select>
					</div>
				</div>
				<div class="field">
					<label for="file">Campaign Image *</label>
					<span class="label-desc">Upload a square image that represents your campaign.</span>
					<div class="row align-items-center pt-3">
						<div class="col-12 file-container">
							<div class="row">
								<div class="col-md-10">
									<p style="color: red">570 x 350 recommended resolution.</p>
									<input type="file" name="file[]" class="form-control" required/>
								</div>
								<div class="col-md-2">
									<button class="btn btn-danger" type="button" onclick="removeFile(this)"> - </button>
								</div>
							</div>
						</div>
						<div class="col-12">
							<br/>
							<button class="btn btn-info" type="button" onclick="addAnotherFile();"> Add Another File </button>
						</div>
					</div>
				</div>
				<div class="field clearfix">
					<label for="city">Campaign Location *</label>
					<span class="label-desc">Choose the location where you are running the campaign.</span>
					<div class="field align-left">
						<input type="text" value="<?php echo set_value('city');?>" id="city" name="city" placeholder="City" required />
					</div>
					<div class="field align-right">
						<div class="field-select">
							<select name="country" required>
								<option value="">Select Country</option>
								<?php
									$country = $this->db->where('country_status',1)->get('country')->result();
									foreach ($country as $item) {
								?>
										<option value="<?php echo $item->country_id;?>"><?php echo $item->country_name;?></option>
								<?php
									}
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="field">
					<label for="category">Campaign Category *</label>
					<span class="label-desc">To help backers find your campaign, select a category that best represents your project.</span>
					<div class="field-select">
						<select name="category" id="category" required>
							<option value="">Select a Category</option>
							<?php
								$categories = $this->db->where('active_status',1)->get('category')->result();
								foreach ($categories as $item) {
							?>
							<option value="<?php echo $item->id;?>"><?php echo $item->catagory_name;?></option>
							<?php
								}
							?>
						</select>
					</div>
				</div>
				<div class="field">
					<label for="raise_amount">How much do you want to raise? <u><?php echo $data['settings']['site_currency'];?></u> *</label>
					<input type="number" id="raise_amount" value="<?php echo set_value('raise_amount');?>" name="raise_amount" placeholder="">
				</div>
				<div class="field">
					<label for="equity_offered">Equity offered *</label>
					<span class="label-desc">The percentage (%) of equity offered for the funding fundraising amount)</span>
					<input type="number" id="equity_offered" value="<?php echo set_value('equity_offered');?>" name="equity_offered" placeholder="">
				</div>
				<div class="field">
					<label for="duration">Campaign End Date *</label>
					<span class="label-desc">You need to provide the campaign closure date.</span>
					<input type="date" id="end_date" value="<?php echo set_value('end_date');?>" name="end_date">
				</div>
				<button type="submit" class="btn-primary saveAndLaunch">Save &amp; Launch</button>
			</form>
		</div>
	</div><!-- .campaign-form -->
	<br/>
	<br/>
	<br/>
</main>
<script>
	function addAnotherFile() {
		$(".file-container").append('<div class="row" style="margin-top: 10px;"> <div class="col-md-10"><p style="color: red">570 x 350 recommended resolution.</p><input type="file" name="file[]" class="form-control"/></div><div class="col-md-2"><button class="btn btn-danger" type="button" onclick="removeFile(this)"> - </button></div></div>');
	}

	function removeFile(e) {
		$(e).parent().parent().remove();
	}

	$( "#end_date" ).datepicker({
		minDate: 0,
		dateFormat: 'yy-mm-dd'
	}); //datepicker("setDate", new Date());
</script>
