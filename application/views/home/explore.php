<script>
	let explore_url = <?php echo json_encode(base_url('home/exploreCampaign/'));?>;
	$(document).on('change',"#filter_by_category",function () {
		window.location.href = explore_url+$(this).val();
	});

	$(document).on('change',"#filter_by_stages",function () {
		window.location.href = explore_url+"0/filter/stages/"+$(this).val();
	});
</script>

<main id="main" class="site-main">
		<div class="page-title background-page">
			<div class="container">
				<h1>Discover projects</h1>
				<div class="breadcrumbs">
					<ul>
						<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
						<li>Projects</li>
					</ul>
				</div><!-- .breadcrumbs -->
			</div>
		</div><!-- .page-title -->
		<div class="campaigns-action clearfix">
			<div class="container">
				<div class="sort">
					<span>Sort by:</span>
					<ul>
						<li class="<?php echo trim($this->uri->segment(5))=='recent' ? 'active' : '';?>"><a href="<?php echo base_url('home/exploreCampaign/0/filter/recent');?>">Recent Project</a></li>
						<li class="<?php echo trim($this->uri->segment(5))!='recent' ? 'active' : '';?>"><a href="<?php echo base_url('home/exploreCampaign/');?>">Most Project</a></li>
					</ul>
				</div><!-- .sort -->
				<div class="filter">
					<span>Filter by:</span>
					<form action="#">
						<div class="field-select">
							<select id="filter_by_stages" name="filter_by_stages">
								<option value="all">All Stages</option>
							</select>
						</div>
						<div class="field-select">
							<select id="filter_by_category" name="filter_by_category">
								<option value="">All Category</option>
								<?php $i=0;
								$categories = $this->db->get("category")->result();
								foreach ($categories as $category) {
								?>
									<option <?php echo !empty($this->uri->segment(3)) && trim($this->uri->segment(3))==$category->id ? 'selected' : '';?> value="<?php echo $category->id;?>"><?php echo $category->catagory_name;?></option>
								<?php
								}
								?>
							</select>
						</div>
					</form>
				</div><!-- .filter -->
			</div>
		</div><!-- .campaigns-action -->
		<div class="campaigns">
			<div class="container">
				<div class="campaign-content">
					<div class="row">
						<div class="col-lg-12">
							<?php
								$random_featured_campaign = $this->db->where('status','live')->where('is_featured',1)->order_by('id', 'RANDOM')->get('campaigns')->row();
								if (empty($random_featured_campaign)){
									$random_featured_campaign = $this->db->where('status','live')->order_by('id', 'RANDOM')->get('campaigns')->row();
								}

								if (!empty($random_featured_campaign)) {

									$images = json_decode($random_featured_campaign->images);
									$firstImage = $images[0];
									$author = $this->db->where('id',$random_featured_campaign->user_id)->get('register')->row();
									$category = $this->db->where('id',$random_featured_campaign->category)->get('category')->row();

									$contributes = $this->db->where('prjt_id',$random_featured_campaign->id)->where('pay_status',1)->get('contribute')->result();
									$funded=$this->db->select_sum('contribute_amount')->where('prjt_id',$random_featured_campaign->id)->where('pay_status',1)->get('contribute')->row()->contribute_amount;

							?>
								<div class="campaign-big-item clearfix">
										<a href="<?php echo base_url('home/campaignDetails/' . $random_featured_campaign->id); ?>" class="campaign-big-image" style="width: 50%">
											<img src="<?php echo base_url($firstImage); ?>" alt=""></a>
										<div class="campaign-big-box">
											<a href="<?php echo base_url('home/campaignDetails/' . $category->id); ?>" class="category"><?php echo $category->catagory_name;?></a>
											<h3>
												<a href="<?php echo base_url('home/campaignDetails/' . $random_featured_campaign->id); ?>">
													<?php echo $random_featured_campaign->title;?>
												</a>
											</h3>
											<div class="campaign-description"><?php echo $random_featured_campaign->introduction;?></div>
											<div class="staff-picks-author">
												<div class="author-profile">
													<a class="author-avatar" href="#">
														<img src="<?php echo !empty($author->profile_image) ? base_url("uploads/user-profile/" . $author->profile_image) : base_url('assets/images/dashboard-avatar.png'); ?>" alt=""></a>by <a
														class="author-name" href="#"><?php echo $author->firstname . ' ' . $author->lastname; ?></a>
												</div>
											</div>
											<div class="process">
												<div class="raised"><span style="width: <?php echo $funded/$random_featured_campaign->raise_amount?>;"></span></div>
												<div class="process-info">
													<div class="process-pledged">
																<span>
																	<?php echo $data['settings']['site_currency'] . ' ' . number_format($random_featured_campaign->raise_amount); ?>
																</span>
														Target
													</div>
													<div class="process-funded">
														<span><?php echo $data['settings']['site_currency'] . ' '.number_format($funded); ?></span>Funded
													</div>
													<div class="process-time"><span>
																	<?php
																	$created_on = strtotime($random_featured_campaign->created_on);
																	$end_date = strtotime($random_featured_campaign->end_date);

																	$date_diff = $end_date - $created_on;
																	$days = round($date_diff / (60 * 60 * 24));

																	echo $days;
																	?>
																</span>Days left
													</div>
												</div>
											</div>
										</div>
									</div>
							<?php
								}
							?>
						</div>

						<?php
						if (empty($data['campaigns'])){
							?>
							<div class="col-md-12">
								<div class="alert alert-info">
									<i class="fa fa-info"></i> No campaigns yet.
								</div>
							</div>
							<?php
						}else {
							$campaigns = $data['campaigns'];
							foreach ( $campaigns as $campaign ) {
								$images = json_decode($campaign->images);
								$firstImage = $images[0];
								$author = $this->db->where('id',$campaign->user_id)->get('register')->row();
								$category = $this->db->where('id',$campaign->category)->get('category')->row();
								$contributes = $this->db->where('prjt_id',$campaign->id)->where('pay_status',1)->get('contribute')->result();
								$funded=$this->db->select_sum('contribute_amount')->where('prjt_id',$campaign->id)->where('pay_status',1)->get('contribute')->row()->contribute_amount;
						?>
								<div class="col-lg-4 col-md-6 col-sm-6 col-6">
									<div class="campaign-item">
										<a class="overlay" href="<?php echo base_url('home/campaignDetails/'.$campaign->id); ?>">
											<img src="<?php echo base_url($firstImage);?>" alt="">
											<span class="ion-ios-search-strong"></span>
										</a>
										<div class="campaign-box">
											<a href="#" class="category"><?php echo !empty($category->catagory_name) ? $category->catagory_name : ""?></a>
											<h3><a href="<?php echo base_url('home/campaignDetails/'.$campaign->id); ?>">
													<?php echo $campaign->title;?></a></h3>
											<div class="campaign-description">
												<?php
													echo $campaign->introduction;
												?>
											</div>
											<div class="campaign-author">
												<a class="author-icon" href="#"><img src="<?php echo !empty($author->profile_image) ?  base_url("uploads/user-profile/".$author->profile_image) : base_url('assets/images/dashboard-avatar.png'); ?>" alt=""></a>by <a
													class="author-name" href="#"><?php echo $author->firstname.' '.$author->lastname;?></a></div>
											<div class="process">
												<div class="raised"><span style="width: <?php echo $funded/$campaign->raise_amount;?>"></span></div>
												<div class="process-info">
													<div class="process-pledged">
														<span>
															<?php echo $data['settings']['site_currency'].' '.number_format($campaign->raise_amount);?>
														</span>
														Target
													</div>
													<div class="process-funded"><span><?php echo $data['settings']['site_currency'].' '.number_format($funded);?></span>Funded</div>
													<div class="process-time">
														<span>
															<?php
															$created_on = strtotime($campaign->created_on);
															$end_date = strtotime($campaign->end_date);

															$date_diff = $end_date - $created_on;
															$days = round($date_diff / (60 * 60 * 24));

															echo $days;
															?>
														</span>Days left
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php
							}
						}
						?>

					</div>
				</div>
			</div>
		</div><!-- .latest -->
	</main><!-- .site-main -->

<br/>
<br/>
<br/>
<br/>
