<style>
	.row {
		width: 100% !important;
	}
	 .company > div > div > p{
		 margin-bottom: 10px!important;
	 }
</style>

<main id="main" class="site-main">
	<div class="page-title background-page">
		<div class="container">
			<h1>Company Overview</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="/">Home</a><span>/</span></li>
					<li>Company Overview</li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="page-content contact-content">
		<div class="container">
			<div class="row">
					<table class="table">
						<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Legal Name</th>
							<th>TIN</th>
							<th>Address</th>
							<th>City</th>
							<th>Country</th>
							<th>Sector</th>
							<th>Action</th>
						</tr>
						</thead>
						<tbody>
						<?php
						$companies_profiles = $this->db->get('companies_profile')->result();
						$i = 1;
						if (!empty($companies_profiles)) {
							foreach ($companies_profiles as $datum) {
								$datum = (object)$datum;
								$companies_profile = $datum;
						?>
								<tr>
									<td><?php echo $i++; ?></td>
									<td><?php echo $datum->name ?></td>
									<td><?php echo $datum->legal_name; ?></td>
									<td><?php echo $datum->tin; ?></td>
									<td><?php echo $datum->address; ?></td>
									<td><?php echo $datum->city; ?></td>
									<td><?php echo $datum->country; ?></td>
									<td><?php echo $datum->sector; ?></td>
									<td>
										<a data-toggle="modal" data-target="#companiesModal<?php echo $i;?>" href="#" class="btn btn-outline-secondary">
											<i class="fa fa-eye">View</i>
										</a>

										<div class="modal fade" id="companiesModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="companiesModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalLabel">View <?php echo $datum->name;?></h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
													</div>
													<div class="modal-body">
														<div class="col-md-12 company">
														<div class="row" style="padding-left: 1%;padding-right: 1%;">

															<div class="col-md-6">
																<label><b>Name of company</b></label>
																<p><?php echo $companies_profile->name; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Legal name</b></label>
																<p><?php echo $companies_profile->legal_name; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>About Company</b></label>
																<p><?php echo $companies_profile->about; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Mission Statement</b></label>
																<p><?php echo $companies_profile->mission; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Vision Statement</b></label>
																<p><?php echo $companies_profile->vision; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Business status</b></label>
																<p><?php echo $companies_profile->business_status; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Tax Identification Number (TIN)</b></label>
																<p><?php echo $companies_profile->tin; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Address</b></label>
																<p><?php echo $companies_profile->address; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>City</b></label>
																<p><?php echo $companies_profile->city; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Country</b></label>
																<p><?php echo $companies_profile->country; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Commencement date</b></label>
																<p><?php echo $companies_profile->commencement_date; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Industry/Sector</b></label>
																<p><?php echo $companies_profile->sector; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Stage</b></label>
																<p><?php echo $companies_profile->stage; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Email</b></label>
																<p><?php echo $companies_profile->email; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Telephone</b></label>
																<p><?php echo $companies_profile->telephone; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Incorporation date</b></label>
																<p><?php echo $companies_profile->incorporation_date; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Facebook</b></label>
																<p><?php echo $companies_profile->facebook; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Linkedin</b></label>
																<p><?php echo $companies_profile->linkedin; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Google</b></label>
																<p><?php echo $companies_profile->google; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Twitter</b></label>
																<p><?php echo $companies_profile->twitter; ?></p>
															</div>
															<div class="col-md-6">
																<label><b>Supporting documents</b></label>
																<p>
																	<?php $i = 1;
																	if (!empty($companies_profile->supporting_docs)) {
																		foreach (json_decode($companies_profile->supporting_docs) as $doc) {
																			?>
																			<a href="<? echo $doc; ?>" target="_blank"
																			   class="btn btn-link"><? echo $i++ . ". View"; ?></a><br/>
																			<?
																		}
																	}
																	?>
																</p>
															</div>

																	<?php
																	$i=1;
																	$management = $this->db->where('user_id', $companies_profile->user_id)->get('managements')->result();
																	if (!empty($management)) {
																		?>
																		<div class="col-md-12">
																			<br/>
																			<br/>
																			<h3>Managements</h3>
																			<br/>
																			<br/>
																			<table class="table">
																				<thead>
																				<tr>
																					<th>#</th>
																					<th>Name</th>
																					<th>TIN</th>
																					<th>Email</th>
																					<th>Telephone</th>
																				</tr>
																				</thead>
																				<tbody>
																				<?php
																				foreach ($management as $manage) {
																					$manage = (object)$manage;
																				?>
																					<tr>
																						<td><?php echo $i++; ?></td>
																						<td><?php echo $manage->title . ' ' . $manage->first_name . ' ' . $manage->surname ?></td>
																						<td><?php echo $manage->tin; ?></td>
																						<td><?php echo $manage->email; ?></td>
																						<td><?php echo $manage->telephone; ?></td>
																					</tr>
																					<?php
																				}
																				?>
																				</tbody>
																			</table>
																		</div>
																		<?php
																	}
																	$i=1;
																	$directors = $this->db->where('user_id', $companies_profile->user_id)->get('directors')->result();
																	if (!empty($directors)) {
																		?>
																		<div class="col-md-12">
																			<br/>
																			<br/>
																			<h3>Directors</h3>
																			<br/>
																			<br/>
																			<table class="table">
																				<thead>
																				<tr>
																					<th>#</th>
																					<th>Name</th>
																					<th>TIN</th>
																					<th>Email</th>
																					<th>Telephone</th>
																				</tr>
																				</thead>
																				<tbody>
																				<?php
																				foreach ($directors as $director) {
																					$director = (object)$director;
																					?>
																					<tr>
																						<td><?php echo $i++; ?></td>
																						<td><?php echo $director->title . ' ' . $director->first_name . ' ' . $director->surname ?></td>
																						<td><?php echo $director->tin; ?></td>
																						<td><?php echo $director->email; ?></td>
																						<td><?php echo $director->telephone; ?></td>
																					</tr>
																					<?php
																				}
																				?>
																				</tbody>
																			</table>
																		</div>
																		<?php
																	}
																	$i=1;
																	$past_investors = $this->db->where('user_id', $companies_profile->user_id)->get('past_investors')->result();
																	if (!empty($past_investors)) {
																		?>
																		<div class="col-md-12">
																			<br/>
																			<br/>
																			<h3>Past Investors</h3>
																			<br/>
																			<br/>
																			<table class="table">
																				<thead>
																				<tr>
																					<th>#</th>
																					<th>Name</th>
																					<th>TIN</th>
																					<th>Job title</th>
																					<th>No. of shares</th>
																					<th>Value of shares</th>
																				</tr>
																				</thead>
																				<tbody>
																				<?php
																				foreach ($past_investors as $past_investor) {
																					$past_investor = (object)$past_investor;
																					?>
																					<tr>
																						<td><?php echo $i++; ?></td>
																						<td><?php echo $past_investor->first_name . ' ' . $past_investor->surname ?></td>
																						<td><?php echo $past_investor->tin; ?></td>
																						<td><?php echo $past_investor->job_title; ?></td>
																						<td><?php echo $past_investor->no_of_shares; ?></td>
																						<td><?php echo $past_investor->value_of_shares; ?></td>
																					</tr>
																					<?php
																				}
																				?>
																				</tbody>
																			</table>
																		</div>
																		<?php
																	}

																	$announcements = $this->db->where('user_id', $companies_profile->user_id)->get('company_announcements')->result();
																	if (!empty($announcements)) {
																	?>
																		<div class="col-md-12">
																			<h2>Announcements</h2>
																			<div class="row">
																				<?php
																					foreach ($announcements as $announcement) {
																				?>
																						<br/>
																						<div class="alert alert-info col-md-12"><?php echo $announcement->content;?></div>
																						<br/>
																				<?php
																					}
																				?>
																			</div>
																		</div>
																		<br/>
																		<br/>
																	<?php
																	}
																	?>
																</div>
														</div>

														</div>
													</div>
												</div>
											</div>
										</div>

									</td>
								</tr>
								<?php
							}
						}
						?>
						</tbody>
					</table>
			</div>

			<br/>
			<br/>
			<br/>
			<br/>

		</div><!-- .container -->
	</div><!-- .page-content -->

</main><!-- .site-main -->
