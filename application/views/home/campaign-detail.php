<?php
	$campaign = $data['campaign'];
	$images = json_decode($campaign->images);
	$author = $this->db->where('id',$campaign->user_id)->get('register')->row();
	$category = $this->db->where('id',$campaign->category)->get('category')->row();
	$country = $this->db->where('country_id',$campaign->country)->get('country')->row();

	$contributes = $this->db->where('prjt_id',$campaign->id)->where('pay_status',1)->get('contribute')->result();

	$funded=$this->db->select_sum('contribute_amount')->where('prjt_id',$campaign->id)->where('pay_status',1)->get('contribute')->row()->contribute_amount;
	$backers=count($contributes);

	$ask_questions = $this->db->where('prjt_id',$campaign->id)->where('active_status',1)->order_by('id','desc')->get('ask_question')->result();

	$companies_profile = $this->db->where('user_id',$campaign->user_id)->get('companies_profile')->row();
?>
<style>
	#company > div > div > p{
		margin-bottom: 10px!important;
	}
</style>
<main id="main" class="site-main">
	<div class="page-title background-campaign">
		<div class="container">
			<h1><?php echo $campaign->title;?></h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo base_url('/');?>">Home</a><span>/</span></li>
					<li><?php echo $campaign->title;?></li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div>
	</div><!-- .page-title -->
	<div class="campaign-content">
		<div class="container">
			<div class="campaign">
				<div class="campaign-item clearfix">
					<div class="campaign-image">
						<div id="owl-campaign" class="campaign-slider">
							<?php
							foreach ($images as $image){
							?>
								<div class="item"><img src="<?php echo base_url($image);?>" alt="Image" style="max-height: 350px"></div>
							<?php
							}
							?>
						</div>
					</div>
					<div class="campaign-box">
						<a href="#" class="category"><?php echo $category->catagory_name;?></a>
						<h3><?php echo $campaign->title;?></h3>
						<div class="campaign-description"><p><?php echo $campaign->introduction;?></p></div>
						<div class="campaign-author clearfix">
							<div class="author-profile">
								<a class="author-icon" href="#"><img src="<?php echo !empty($author->profile_image) ?  base_url("uploads/user-profile/".$author->profile_image) : base_url('assets/images/dashboard-avatar.png'); ?>" alt=""></a>by <a class="author-name" href="#"><?php echo $author->firstname.' '.$author->lastname;?></a>
							</div>
							<div class="author-address"><span class="ion-location"></span><?php echo $campaign->city.' '.$country->country_name;?></div>
						</div>
						<div class="process">
							<div class="raised"><span style="width: <?php echo $funded/$campaign->raise_amount;?>%"></span></div>
							<div class="process-info">
								<div class="process-funded"><span><?php echo $data['settings']['site_currency'].' '.number_format($campaign->raise_amount);?></span>funding goal</div>
								<div class="process-pledged"><span><?php echo $data['settings']['site_currency'].' '.number_format($funded);?></span>funded</div>
								<div class="process-time"><span><?php echo $backers;?></span>backers &nbsp;&nbsp;</div>
								<div class="process-time"><span>
									<?php
										$created_on = strtotime($campaign->created_on);
										$end_date = strtotime($campaign->end_date);

										$date_diff = $end_date - $created_on;
										$days = round($date_diff / (60 * 60 * 24));

										echo $days;
									?>
								</span>days left</div>
							</div>
						</div>
						<div class="button">
							<?php
								if ($days <= 0){
							?>
									<button class="btn-secondary"><i class="fa fa-clock-o"></i>Closed</button>
							<?php
								}else {
									if ($this->Auth_model->is_loggedIn()) {
										$user_data = $this->Auth_model->getLoggedInUserData();
										if ($user_data->id == $campaign->user_id){
							?>
											<p class="alert alert-info">This is your Project!!</p>
											<br/>
											<button class="btn-primary" onclick="alert('This is your Project!!')" type="submit">Back this Campaign</button>
											<button class="btn-secondary" onclick="alert('This is your Project!!')"><i class="fa fa-heart" aria-hidden="true"></i>Follow</button>
							<?php
										}else {
										$contribute = $this->db->where('prjt_id',$campaign->id)->where('user_id',$campaign->user_id)->get('contribute')->result();
											if (!empty($contribute) && count($contribute) > 0){
							?>
												<p class="alert alert-info">You have already Invested.</p>
												<br/>
							<?php
											}else {

												$follow = $this->db->where('prjt_id',$campaign->id)->where('user_id',$this->session->userdata('eqty_userid'))->get('follow')->row();

												$follow_msg="Do you want to proceed and add this campaign to your following list?";
												$btn_name="Follow";
												$f_url = base_url('home/follow/'.$campaign->id);

												if (!empty($follow)){
													$follow_msg="Do you want to proceed and remove this campaign from your following list?";
													$btn_name="UnFollow";
													$f_url = base_url('home/unfollow/'.$campaign->id);
												}
												?>
												<form method="post" action="<?php echo base_url('home/makeInvestment')?>" id="priceForm" class="campaign-price quantity">
													<input type="number" value="" min="1" name="amount" placeholder="" required/>
													<input type="hidden" name="campaign_id" value="<?php echo $campaign->id;?>"/>
													<button class="btn-primary" type="submit">Back this Campaign</button>
												</form>
												<a href="<?php echo $f_url;?>" class="btn-secondary" onclick="confirm('<?php echo $follow_msg;?>')">
													<i class="fa fa-heart" aria-hidden="true"></i><?php echo $btn_name;?></a>
												<?php
											}
										}
									}else{
							?>
										<a href="<?php echo base_url('auth/login');?>" class="btn-secondary"><i class="fa fa-lock" aria-hidden="true"></i>Login to back this campaign</a>
							<?php
									}
								}
							?>
						</div>
						<div class="share">
							<p>Share this project</p>
							<ul>
								<li class="share-facebook"><a href="http://www.facebook.com/sharer.php?u=<?php echo base_url('campaignDetails/'.$campaign->id);?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li class="share-twitter"><a href="https://twitter.com/share?url=<?php echo base_url('campaignDetails/'.$campaign->id);?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li class="share-google-plus"><a href="https://plus.google.com/share?url=<?php echo base_url('campaignDetails/'.$campaign->id);?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
								<li class="share-linkedin"><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo base_url('campaignDetails/'.$campaign->id);?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- .campaign-content -->
	<div class="campaign-history">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="campaign-tabs">
						<ul class="tabs-controls">
							<li class="active" data-tab="campaign"><a href="#">Campaign Story</a></li>
							<?php
							if (!empty($companies_profile)) {
							?>
								<li data-tab="company"><a href="#">Company Overview</a></li>
							<?php
							}
							?>
							<li data-tab="backer"><a href="#">Backer List</a></li>
							<li data-tab="comment"><a href="#">Comments</a></li>
						</ul>
						<div class="campaign-content">
							<div id="campaign" class="tabs active">

								<h4>Introduction</h4>
								<p><?php echo $campaign->introduction;?></p>
								<br/>

								<h4>Problem</h4>
								<p><?php echo $campaign->problem;?></p>
								<br/>

								<h4>Solution</h4>
								<p><?php echo $campaign->problem;?></p>
								<br/>

								<h4>Intended Results</h4>
								<p><?php echo $campaign->intended_results;?></p>
								<br/>

								<h4>Accomplishment So Far</h4>
								<p><?php echo $campaign->accomplishment_so_far;?></p>
								<br/>

								<h4>Revenue Strategy</h4>
								<p><?php echo $campaign->revenue_strategy;?></p>
								<br/>

								<h4>Investment Terms</h4>
								<p><?php echo $campaign->investment_terms;?></p>
								<br/>

							</div>

							<?php
								if (!empty($companies_profile)) {
							?>
									   <div id="company" class="tabs">
										<div class="row">
											<div class="col-md-6">
												<label><b>Name of company</b></label>
												<p><?php echo $companies_profile->name; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Legal name</b></label>
												<p><?php echo $companies_profile->legal_name; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>About Company</b></label>
												<p><?php echo $companies_profile->about; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Mission Statement</b></label>
												<p><?php echo $companies_profile->mission; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Vision Statement</b></label>
												<p><?php echo $companies_profile->vision; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Business status</b></label>
												<p><?php echo $companies_profile->business_status; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Tax Identification Number (TIN)</b></label>
												<p><?php echo $companies_profile->tin; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Address</b></label>
												<p><?php echo $companies_profile->address; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>City</b></label>
												<p><?php echo $companies_profile->city; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Country</b></label>
												<p><?php echo $companies_profile->country; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Commencement date</b></label>
												<p><?php echo $companies_profile->commencement_date; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Industry/Sector</b></label>
												<p><?php echo $companies_profile->sector; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Stage</b></label>
												<p><?php echo $companies_profile->stage; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Email</b></label>
												<p><?php echo $companies_profile->email; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Telephone</b></label>
												<p><?php echo $companies_profile->telephone; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Incorporation date</b></label>
												<p><?php echo $companies_profile->incorporation_date; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Facebook</b></label>
												<p><?php echo $companies_profile->facebook; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Linkedin</b></label>
												<p><?php echo $companies_profile->linkedin; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Google</b></label>
												<p><?php echo $companies_profile->google; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Twitter</b></label>
												<p><?php echo $companies_profile->twitter; ?></p>
											</div>
											<div class="col-md-6">
												<label><b>Supporting documents</b></label>
												<p>
													<?php $i = 1;
													if (!empty($companies_profile->supporting_docs)) {
														foreach (json_decode($companies_profile->supporting_docs) as $doc) {
															?>
															<a href="<? echo $doc; ?>" target="_blank"
															   class="btn btn-link"><? echo $i++ . ". View"; ?></a><br/>
															<?
														}
													}
													?>
												</p>
											</div>
										</div>
										<?php
										$i=1;
										$management = $this->db->where('user_id', $campaign->user_id)->get('managements')->result();
										if (!empty($management)) {
											?>
											<div class="row">
												<br/>
												<br/>
												<h3>Managements</h3>
												<br/>
												<br/>
												<table class="table">
													<thead>
													<tr>
														<th>#</th>
														<th>Name</th>
														<th>TIN</th>
														<th>Email</th>
														<th>Telephone</th>
													</tr>
													</thead>
													<tbody>
													<?php
													foreach ($management as $datum) {
														$datum = (object)$datum;
														?>
														<tr>
															<td><?php echo $i++; ?></td>
															<td><?php echo $datum->title . ' ' . $datum->first_name . ' ' . $datum->surname ?></td>
															<td><?php echo $datum->tin; ?></td>
															<td><?php echo $datum->email; ?></td>
															<td><?php echo $datum->telephone; ?></td>
														</tr>
														<?php
													}
													?>
													</tbody>
												</table>
											</div>
										<?php
										}
										$i=1;
										$directors = $this->db->where('user_id', $campaign->user_id)->get('directors')->result();
										if (!empty($directors)) {
										?>
											<div class="row">
											<br/>
											<br/>
											<h3>Directors</h3>
											<br/>
											<br/>
											<table class="table">
												<thead>
												<tr>
													<th>#</th>
													<th>Name</th>
													<th>TIN</th>
													<th>Email</th>
													<th>Telephone</th>
												</tr>
												</thead>
												<tbody>
												<?php
												foreach ($directors as $datum) {
													$datum = (object)$datum;
													?>
													<tr>
														<td><?php echo $i++; ?></td>
														<td><?php echo $datum->title . ' ' . $datum->first_name . ' ' . $datum->surname ?></td>
														<td><?php echo $datum->tin; ?></td>
														<td><?php echo $datum->email; ?></td>
														<td><?php echo $datum->telephone; ?></td>
													</tr>
													<?php
												}
												?>
												</tbody>
											</table>
										</div>
										<?php
										}
										$i=1;
										$past_investors = $this->db->where('user_id', $campaign->user_id)->get('past_investors')->result();
										if (!empty($past_investors)) {
										?>
										<div class="row">
											<br/>
											<br/>
											<h3>Past Investors</h3>
											<br/>
											<br/>
											<table class="table">
												<thead>
												<tr>
													<th>#</th>
													<th>Name</th>
													<th>TIN</th>
													<th>Job title</th>
													<th>No. of shares</th>
													<th>Value of shares</th>
												</tr>
												</thead>
												<tbody>
												<?php
												foreach ($past_investors as $datum) {
													$datum = (object)$datum;
													?>
													<tr>
														<td><?php echo $i++; ?></td>
														<td><?php echo $datum->first_name . ' ' . $datum->surname ?></td>
														<td><?php echo $datum->tin; ?></td>
														<td><?php echo $datum->job_title; ?></td>
														<td><?php echo $datum->no_of_shares; ?></td>
														<td><?php echo $datum->value_of_shares; ?></td>
													</tr>
													<?php
												}
												?>
												</tbody>
											</table>
										</div>
										<?php
										}
										?>
									   </div>
								   <?php
									}
								   ?>

							<div id="backer" class="tabs">
								<table>
									<tr>
										<th>Name</th>
										<th>Donate Amount</th>
										<th>Date</th>
									</tr>
									<?php
										foreach ($contributes as $contribute) {
											$user = $this->db->where('id',$contribute->user_id)->get('register')->row();
									?>
											<tr>
												<td><?php echo empty($user) ? "Unknown" : $user->firstname.' '.$user->lastname;?></td>
												<td><?php echo $data['settings']['site_currency'].' '.number_format($contribute->contribute_amount);?></td>
												<td><?php echo date('M d, Y',strtotime($contribute->date));?></td>
											</tr>
									<?php
										}
									?>
								</table>
							</div>
							<div id="comment" class="tabs comment-area">
								<h3 class="comments-title"><?php echo !empty($ask_questions) ? count($ask_questions) : 0;?> Comment</h3>
								<ol class="comments-list">
									<?php
										if (!empty($ask_questions)) {
											foreach ($ask_questions as $ask_question) {
												$author = $this->db->where('id',$ask_question->user_id)->get('register')->row();
									?>
												<li class="comment clearfix">
													<div class="comment-body">
														<div class="comment-avatar">
															<img src="<?php echo !empty($author->profile_image) ?  base_url("uploads/user-profile/".$author->profile_image) : base_url('assets/images/dashboard-avatar.png'); ?>" style="padding: 2%" alt=""/>
														</div>
														<div class="comment-info">
															<header class="comment-meta"></header>
															<cite class="comment-author"><?php echo $author->firstname.' '.$author->lastname;?></cite>
															<div class="comment-inline">
																<span class="comment-date"><?php echo date('d-m-Y',strtotime($ask_question->ques_dt));?></span>
																<?php
																	if (empty($ask_question->ans)){
																?>
																	<a data-toggle="collapse" href="#collapseExample<?php echo $campaign->id;?>" class="comment-reply">Reply</a>
																<?php
																	}
																?>
															</div>
															<div class="comment-content">
																<p><?php echo stripslashes($ask_question->ques);?></p>
																<?php
																if (!empty($ask_question->ans)){
																?>
																	<br/>
																	<div class="alert alert-info">
																		<b>Reply</b>
																		<p><?php echo ucfirst(stripslashes($ask_question->ans));?></p>
																	</div>
																	<br/>
																	<?php
																}else{
																?>
																	<div class="collapse" id="collapseExample<?php echo $campaign->id;?>">
																		<br/>
																		<div class="well">
																			<div class="row">
																				<div class="col-md-12">
																					<form method="post" action="" class="form-horizontal product-form">
																						<input type="hidden" name="post_comment_id" value="<?php echo $ask_question->id;?>" />
																						<div class="form-group">
																							<label for="reply">Reply Comments </label>
																							<textarea id="reply" name="reply_comm" class="form-control" rows="5" required="required"></textarea>
																						</div>
																						<div class="row">
																							<button style="background: #0c5460;padding: 1%;color:white;">Post Reply </button>
																						</div>
																					</form>
																				</div>
																			</div>
																		</div>
																	</div>
																<?php
																}
																?>
															</div>
														</div>
													</div>
												</li>
									<?php
											}
										}
									?>
								</ol>
								<div id="respond" class="comment-respond">
									<?php
									if (empty($this->session->userdata('eqty_userid'))){
									?>
										<div class="row text-center">
											<div class="well">
												<a href="<?php echo base_url('auth/login'); ?>" class="btn btn-primary">Login To Post Comment</a>
											</div>
										</div>
									<?php
									}elseif ($campaign->user_id == $this->session->userdata('eqty_userid')){
									?>
										<div class="row text-center">
											<div class="alert alert-info">
												This is your project
											</div>
										</div>
									<?php
									}else {
									?>
										<h3 id="reply-title" class="comment-reply-title">Leave A Comment?</h3>
										<form method="post" action="#" id="commentForm">
											<div class="field-textarea">
												<textarea rows="8" name="ques" placeholder="Post your comments here..." required></textarea>
											</div>
											<button type="submit" value="Post Comment" class="btn-primary">Post
												Comment
											</button>
										</form>
									<?php
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div><!-- .main-content -->
				<div class="col-lg-4">
					<div class="support support-campaign">
						<h3 class="support-campaign-title">Back other campaigns from this author</h3>
						<?php
							$other_campaigns = $this->db->where('id != ',$campaign->id)->where('user_id',$campaign->user_id)
												->where('status','live')->get('campaigns')->result();
							if (!empty($other_campaigns)) {
								foreach ($other_campaigns as $other_campaign) {

									$contributes = $this->db->where('prjt_id',$other_campaign->id)->where('pay_status',1)->get('contribute')->num_rows();
							?>
									<div class="plan">
										<a href="<?php echo base_url('home/campaignDetails/' . $other_campaign->id); ?>">
											<h4><?php echo $other_campaign->title;?></h4>
											<div class="plan-desc"><p><?php echo $other_campaign->introduction;?></p>
											</div>
											<div class="plan-date"><?php echo date('M d, Y',strtotime($other_campaign->end_date));?></div>
											<div class="plan-author">Estimated Delivery</div>
											<div class="backer"><?php echo $contributes;?> backer</div>
										</a>
									</div>
						<?php
								}
							}else{
						?>
							<div class="alert alert-info">
								<i class="fa fa-info"></i> Author does not have other campaigns
							</div>
						<?php
							}
						?>
					</div>
				</div><!-- .sidebar -->
			</div>
		</div>
	</div><!-- .campaign-history -->
</main><!-- .site-main -->
<script>

</script>
