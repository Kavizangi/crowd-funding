<?php

$PAYSTACK_PUBLIC_KEY="pk_test_ae8679db075dfd9b15443f82b7fc550ffbe035aa";
$PAYSTACK_SECRET_KEY="sk_test_9061db60ed6fb90692cea28bafed5da783b103ef";

$amt = $this->db->query("select contribute_amount from contribute where id='".$data['id']."'")->row()->contribute_amount;
$email = $this->Auth_model->getLoggedInUserData()->email;

$return_url = base_url("home/paymentSuccess/");
$cancel = base_url("home/paymentCancel");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Paystack Payment Gateway</title>
</head>
<body></body>
</html>

<script src='https://js.paystack.co/v1/inline.js'></script>
<script type='text/javascript'>
	submitPaystackPop();
	function submitPaystackPop(){
		let handler = PaystackPop.setup({
			key: '<?php echo $PAYSTACK_PUBLIC_KEY; ?>',
			email: '<?php echo $email;?>',
			amount: "<?php echo ($amt*100); ?>",
			currency: 'NGN',
			ref: '<?php echo $_SESSION['invid']; ?>',
			callback: function(response) {
				console.log(response);
				if(response.status == "success"){
					window.location.href = "<? echo $return_url;?>";
				}else{
					window.location.href = "<? echo $cancel;?>";
				}
			},
			onClose: function() {
				alert('Window closed');
				history.go(-1);
			}
		});
		handler.openIframe();
	}
</script>
