<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('site_settings'))
{
	function site_settings()
	{
		$ci=& get_instance();
		$ci->load->database();

		$options = $ci->db->query("SELECT * FROM `options`")->result();
		$data = array();
		foreach($options as $opt) {
			$data[$opt->option_name] = $opt->option_value;
		}

		return $data;
	}
}
