<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{

	private $settings = array();
	private $data = array();

	public function __construct()
	{
		parent::__construct();
		$this->settings = site_settings();
		$this->data['settings'] = $this->settings;
	}

	public function login(){
		$this->index();
	}

	public function index()
	{
		if ($this->Auth_model->is_loggedIn()){
			return redirect('user');
		}

		#Loggin user
		if($_SERVER['REQUEST_METHOD'] == 'POST'){

			if( empty($this->input->post('email',true)) || empty($this->input->post('password',true)) ) {
				$this->session->set_flashdata("login_fail","Oops! Fill out the empty fields!");
			} else {
				$user_result = $this->db->where(array('email'=>$this->input->post('email',true),
												'repassword'=>md5($this->input->post('password',true))))
													->get('register');

				if($user_result->num_rows() > 0) {
					$query = (array) $user_result->row();
					$user_id = $query['id'];
					$email_sts = $query['email_active_status'];
					$active_sts = $query['active_status'];

					if($active_sts == 1) {
						if($email_sts == 1) {
							$this->session->set_userdata('eqty_userid',$user_id);
							$this->session->set_userdata('eqty_mail',$this->input->post('email',true));

							$login_date = date('Y-m-d H:i:s');
							$this->db->query("update register SET last_login_date='$login_date',login_ip_addr='' WHERE id='$user_id'");
							redirect("user/index");
						} else {
							$this->session->set_flashdata("login_fail","Oops! Kindly activate your account via Email!");
						}
					} else {
						$this->session->set_flashdata("login_fail","Oops! Your account is inactive!");
					}
				} else {
					$this->session->set_flashdata("login_fail","Oops! Invalid Credentials!");
				}
			}

		}

		$this->data['page'] = "auth/login";
		$this->data['page_title'] = "Login Page";
		$this->data['page_name'] = "login";
		$this->data['data'] = $this->data;

		$this->load->view('home/layout/app', $this->data);
	}

	public function register()
	{
		if ($this->Auth_model->is_loggedIn()){
			return redirect('user');
		}

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){

			if(!filter_var($this->input->post('email',true), FILTER_VALIDATE_EMAIL)) {
				$this->session->set_flashdata("login_fail","Invalid Email!");
			}
			else if(strlen($this->input->post('password',true)) < 6) {
				$this->session->set_flashdata("login_fail","Length of password atleast 6 characters");
			}
			else if( $this->input->post('confirm_password',true) != $this->input->post('password',true)) {
				$this->session->set_flashdata("login_fail","Password does not match!");
			}
			else {
				$result = $this->db->where('email',$this->input->post('email',true))->get('register');
				$exist_email = $result->num_rows();
				if($exist_email==0)
				{
					$id1=uniqid();
					$id2=rand(0,100);
					$activation_key=$id1 . $id2;
					$crcdt= date('Y-m-d H:i:s');
					$password=md5($this->input->post('pass',true));
					$insert = array(
						'firstname' => $this->input->post('first_name',true),
						'lastname' => $this->input->post('last_name',true),
						'email' => $this->input->post('email',true),
						'password' => $this->input->post('password',true),
						'repassword' => $password,
						'phone' => $this->input->post('mobile_no',true),
						'user_type' => '0',
						'tmp_key' => $activation_key,
						'active_status' => 1,
						'reg_ip_addr'=>'',
						'crcdt'=>$crcdt
					);

					$this->db->insert('register',$insert);

					$insert_id = $this->db->insert_id();
					$site_logo = base_url("uploads/settings/".$this->settings['site_logo']);
					$top_content="Welcome to the ".$this->settings['site_title']."!";
					$encode_id = base64_encode($insert_id);
					$redirect_url = base_url("auth/login?code=".$activation_key."&lgid=".$encode_id);

					$sign_up_msg=signupTemplate(base_url('/'),$site_logo,$top_content,$this->settings['site_title'],$redirect_url);
					$subject="Activate your"." ".$this->settings['site_title']." "."account";
					$result=sendEmail($this->settings['admin_email'],$insert['email'],$subject,$sign_up_msg);
					if($result == "scs") {
						$this->session->set_flashdata("login_success","Registered Successfully! We will send conformation message shortly.Kindly check your mail.");
					}
					else {
						$this->session->set_flashdata("login_fail","Oops! There is a problem with sending email.Try again later");
					}
				}
				else {
					$this->session->set_flashdata("login_fail","Email ID already exists!");
				}
			}

		}

		$this->data['page'] = "auth/register";
		$this->data['page_title'] = "Register Page";
		$this->data['page_name'] = "register";
		$this->data['data'] = $this->data;

		$this->load->view('home/layout/app', $this->data);
	}

	public function accessdenied(){
		exit("access denied");
	}

	public function logout(){
		$this->session->unset_userdata('eqty_userid');
		$this->session->unset_userdata('eqty_mail');
		return redirect('auth/login');
	}
}
