<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	private $settings=array();
	private $data=array();
	private $dateTimeNow;

	public function __construct()
	{
		parent::__construct();
		$this->settings = site_settings();
		$this->data['settings'] = $this->settings;
		$this->dateTimeNow = date('Y-m-d H:i:s');
	}

	public function unfollow($campaign_id){
		if ($this->Auth_model->is_loggedIn()) {
			$this->db->where('prjt_id', $campaign_id)->where('user_id', $this->session->userdata('eqty_userid'))->delete("follow");
		}
		redirect($this->agent->referrer());
	}

	public function follow($campaign_id){
		if ($this->Auth_model->is_loggedIn()) {
			$this->db->where('prjt_id', $campaign_id)->where('user_id', $this->session->userdata('eqty_userid'))->delete("follow");
			$this->db->insert('follow',['prjt_id'=>$campaign_id,'user_id'=>$this->session->userdata('eqty_userid')]);
		}
		redirect($this->agent->referrer());
	}

	public function stateAjax($state_id){
		$state_html = "<option value=''>Select State</option>";
		$states = $this->db->where('state_country_id',$state_id)->where('state_status',1)->get('state')->result();
		foreach ($states as $state) {
			$state_html.="<option value='".$state->state_id."'>".$state->state_name."</option>";
		}
		echo $state_html;
	}

	public function cityAjax($city_id){
		$city_html = "<option value=''>Select City</option>";
		$cities = $this->db->where('city_state_id',$city_id)->where('city_status',1)->get('city')->result();
		foreach ($cities as $city) {
			$city_html.="<option value='".$city->city_id."'>".$city->city_name."</option>";
		}
		echo $city_html;
	}

	public function index(){
		$this->data['page']="home/index";
		$this->data['page_title']="Home Page";
		$this->data['page_name']="home";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function makeInvestment(){

		if (!$this->Auth_model->is_loggedIn()) {
			redirect($this->agent->referrer());
		}

		if (empty($this->input->post('amount',true)) && empty($this->input->post('campaign_id',true))){
			redirect($this->agent->referrer());
		}

		$this->data['page']="home/make-investment";
		$this->data['page_title']="Make Investment";
		$this->data['page_name']="make-investment";
		$this->data['amount'] = $this->input->post('amount',true);
		$this->data['campaign_id'] = $this->input->post('campaign_id',true);
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	private function savePayment($pay_type){

		$array = array(
			'prjt_id'=>$this->input->post('campaign_id',true),
			'user_id'=>$this->session->userdata('eqty_userid'),
			'contribute_amount'=>$this->input->post('amount',true),
			'pay_type'=>$pay_type,
			'date'=>$this->dateTimeNow
		);

		$this->db->insert('contribute',$array);

		return $this->db->insert_id();
	}

	public function doPayment(){

		if (!$this->Auth_model->is_loggedIn()) {
			redirect($this->agent->referrer());
		}

		if (empty($this->input->post('radio',true))){
			redirect($this->agent->referrer());
		}

		$payment_methods = array('paystack','paypal','bank');

		if (!in_array($this->input->post('radio',true), $payment_methods)){
			redirect($this->agent->referrer());
		}

		if (empty($this->input->post('amount',true)) || empty($this->input->post('campaign_id',true))){
			redirect($this->agent->referrer());
		}

		$to_email = $this->Auth_model->getLoggedInUserData()->email;
		$prjt_title = ucwords(stripslashes($this->db->where('id',$this->input->post('campaign_id',true))->get('campaigns')->row()->title));
		$topcontent = "<br /><center><div style='background-color:#f1f1f1;width:500px;padding:10px;'> <span style='color:#d779bf;'>Your have invested successfully.";
		$topcontent .="<br><br /> <span style='color:#7a79d7;'>Here is the Your Investment Details,</span>";
		$topcontent .="<table class='mail-tabl'>
	<tr><td> Project  </td><td>:</td><td> $prjt_title</td></tr>
	<tr><td> Investment Amount  </td><td>:</td><td> ".$this->settings['site_currency'].$this->input->post('amount',true)."</td></tr>
	</table></div>";

		if ($this->input->post('radio',true) == "paystack"){
			return $this->paystack($this->savePayment("paystack"));
		}else if ($this->input->post('radio',true) == "paypal"){
			return $this->paypal($this->savePayment("paypal"));
		}else{

			$ad_bank_details = $this->db->get('ad_bank_details')->row();

			$topcontent .="<div style='background-color:#f1f1f1;width:500px;padding:10px;'><center><br><br><span style='color:#7a79d7;'>Here is the Your Payment Details,";
			$topcontent .="<br>You can Pay through Bank with below Details,</span>";
			$topcontent .="<table><tr><td> Bank Name  </td><td>:</td><td> $ad_bank_details->bank_name</td></tr>
		<tr><td> Branch Name  </td><td>:</td><td> $ad_bank_details->branch_name</td></tr>
		<tr><td> Account Holder Name  </td><td>:</td><td> $ad_bank_details->acct_name</td></tr>
		<tr><td> Account No.  </td><td>:</td><td> $ad_bank_details->account_num</td></tr>
		<tr><td> SWIFT Code  </td><td>:</td><td> $ad_bank_details->ifsc</td></tr>
		</table></center>";
			$topcontent .="<br>After the payment, <a href='".base_url('auth/login')."'>Click Here</a> to login & upload your payslip to confirm your payment.</div>";

			$this->savePayment("bank");
		}

		$cur_url = base_url('/');
		$site_logo_lnk = base_url("/uploads/settings/".$this->settings['site_logo']);
		$specific_title = 'Login to check your investment status';
		$btn_name = 'Click Here';
		$msg = customTemplate(base_url('/'),$site_logo_lnk,$topcontent,$this->settings['site_title'],$cur_url,$specific_title,$btn_name);
		$subject = 'Project Investment report from '.$this->settings['site_title'];
		sendEmail($this->settings['admin_email'],$to_email,$subject,$msg);

		echo '<script language="javascript">' .
			'alert("Thank You! Your investment was successful. Kindly check your email");' .
			'setTimeout(function(){ window.location.href = "'.base_url('user/myInvestments').'"; }, 3000);' .
			'</script>';
	}

	private function paypal($id){
		$_SESSION["invid"]=$id;
		$this->data['id']=$id;
		$this->data['settings'] = $this->settings;
		$this->data['data']=$this->data;
		$this->load->view('home/paypal',$this->data);
	}

	private function paystack($id){
		$_SESSION["invid"]=$id;
		$this->data['id']=$id;
		$this->data['data']=$this->data;
		$this->load->view('home/paystack',$this->data);
	}

	public function about(){
		$this->data['page']="home/about";
		$this->data['page_title']="About Us";
		$this->data['page_name']="about";
		$this->data['settings'] = $this->settings;
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function paymentSuccess(){
		$invid = $_SESSION["invid"];
		$this->db->query("update contribute set pay_status='1' where invest_id='$invid'");
	}

	public function paymentCancel(){
		$invest_id = $_SESSION['invid'];
		$this->db->query("delete from contribute where invest_id=".$invest_id);
		unset($_SESSION["invid"]);
		redirect('user/myInvestments');
	}

	public function companies(){
		$this->data['page']="home/companies";
		$this->data['page_title']="Companies";
		$this->data['page_name']="companies";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function campaignDetails($campaign_id=0){
		$this->data['page']="home/campaign-detail";
		$this->data['page_title']="Campaign Detail";
		$this->data['page_name']="campaign-detail";

		$campaigns = $this->db->where('status','live')->where('id',$campaign_id)->get('campaigns');
		if ($campaigns->num_rows() == 0){
			redirect('/');
		}

		$this->data['campaign']=$campaigns->row();
		$user_id = $this->data['campaign']->user_id;

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {

			if (!empty($this->input->post('post_comment_id',true))){
				$array = array(
					'ans' => $this->input->post('reply_comm',true),
					'ans_dt' =>$this->dateTimeNow
				);
				$this->db->where('id',$this->input->post('post_comment_id',true))->update('ask_question',$array);
			}

			if (!empty($this->input->post('ques',true))){
				$array = array(
					'ques' => $this->input->post('ques',true),
					'ques_dt'=>$this->dateTimeNow,
					'user_id'=>$user_id,
					'prjt_id'=>$campaign_id,
					'active_status'=>0
				);
				$this->db->insert('ask_question',$array);

				$author = $this->db->where('id',$user_id)->get('register')->row();

				$site_logo_lnk = base_url("/uploads/settings/".$this->settings['site_logo']);
				$topcontent = "User Comments about your project. The user comment was: ";
				$topcontent .= "<br/><b>".$array['ques']."</b>";
				$specific_title = 'Login to view & reply the comment';
				$btn_name = 'Click Here';
				$msg = customTemplate(base_url('/'),$site_logo_lnk,$topcontent,$this->settings['site_title'],base_url('/'),$specific_title,$btn_name);
				$subject = 'User Comments from '.$this->settings['site_title'];
				sendEmail($this->settings['admin_email'],$author->email,$subject,$msg);

			}
		}

		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function exploreCampaign($category_id=0,$filter="",$filter_by="",$value=""){
		$this->data['page']="home/explore";
		$this->data['page_title']="Explore";
		$this->data['page_name']="explore";

		if (!empty($category_id)){
			$this->db->where('category',$category_id);
		}

		if (!empty($filter) && trim($filter_by)=="stages"){
			if (!empty($value)) {
				$this->db->where('status', $value);
			}
		}

		if (!empty($filter) && trim($filter_by)=="recent"){
			$this->db->order_by('created_on','desc');
		}

		$this->data['campaigns']=$this->db->where('status','live')->get('campaigns')->result();
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function startCampaign(){

		if (!$this->Auth_model->is_loggedIn()){
			return redirect('auth/login');
		}

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){

			$images_uploaded = array();

			if ( !empty($_FILES["file"]) && count($_FILES["file"]["name"]) >  0 ) {

				for ($i=0; $i < count($_FILES["file"]["name"]); $i++) {

					$target_dir = "uploads/campaigns";
					$target_file = $target_dir . time() . basename($_FILES["file"]["name"][$i]);
					$file_type = $_FILES["file"]["type"][$i];
					$post_tmp = $_FILES["file"]["tmp_name"][$i];

					switch ($file_type) {
						case 'image/jpeg':
						case 'image/gif':
						case 'image/png':

							if (is_dir($target_dir) && is_writable($target_dir)) {

								if (move_uploaded_file($post_tmp, $target_file)) {
									array_push($images_uploaded, $target_file);
								}

							}
					}

				}

			}

			if ( count($images_uploaded) == 0 ){
				$this->session->set_flashdata('create_campaign_failed',"Uploading the images failed, upload only images.");
			}else {

				$created_on = strtotime($this->dateTimeNow);
				$end_date = strtotime($this->input->post('end_date', true));

				$date_diff = $end_date - $created_on;
				$days = round($date_diff / (60 * 60 * 24));

				$fields = array(
					'title' => $this->input->post('title', true),
					'introduction' => $this->input->post('introduction', true),
					'problem' => $this->input->post('problem', true),
					'solution' => $this->input->post('solution', true),
					'intended_results' => $this->input->post('intended_results', true),
					'accomplishment_so_far' => $this->input->post('accomplishment_so_far', true),
					'revenue_strategy' => $this->input->post('revenue_strategy', true),
					'use_of_funds' => $this->input->post('use_of_funds', true),
					'investment_terms' => $this->input->post('investment_terms', true),
					'city' => $this->input->post('city', true),
					'country' => $this->input->post('country', true),
					'category' => $this->input->post('category', true),
					'raise_amount' => $this->input->post('raise_amount', true),
					'duration' => $days,
					'end_date' => $end_date,
					'equity_offered' => $this->input->post('equity_offered',true),
					'user_id' => $this->session->userdata('eqty_userid'),
					'status' => 'pending',
					'images' => json_encode($images_uploaded),
					'updated_on' => $this->dateTimeNow,
					'created_on' => $this->dateTimeNow,
				);

				if ( $this->db->insert('campaigns',$fields) ){
					redirect('user/campaign');
				}else{
					$this->session->set_flashdata('create_campaign_failed',"Could not create the campaign, retry..");
				}

			}
		}

		$this->data['page']="home/create-campaign";
		$this->data['page_title']="Start a Campaign";
		$this->data['page_name']="create-campaign";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function privacyPolicy(){
		$this->data['page']="home/privacy-policy";
		$this->data['page_title']="Privacy Policy";
		$this->data['page_name']="privacy-policy";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function faq(){
		$this->data['page']="home/faq";
		$this->data['page_title']="FAQ";
		$this->data['page_name']="faq";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function contact(){

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){

			$insert = array(
				'name'=>$this->input->post('name',true),
				'email'=>$this->input->post('email',true),
				'subject'=>$this->input->post('subject',true),
				'comment'=>$this->input->post('message',true)
			);

			$site_logo_lnk = base_url("/uploads/settings/".$this->settings['site_logo']);
			$specific_title = '';
			$btn_name = 'Click Here to visit the Site';
			$text_to_admin = "<table>
				<tr>
					<td>Name </td>
					<td> : &nbsp;</td>
					<td>".$insert['name']."</td>
				</tr>
				<tr>
					<td>Email </td>
					<td> : &nbsp;</td>
					<td> ".$insert['email']."</td>
				</tr>
				<tr>
					<td>Subject </td>
					<td> : &nbsp;</td>
					<td> ".$insert['comment']."</td>
				</tr>
				<tr>
					<td>Comment </td>
					<td> : &nbsp;</td>
					<td> ".$insert['comment']."</td>
				</tr>
				</table>";

			$msg_to_admin = customTemplate(base_url('/'),$site_logo_lnk,$text_to_admin,$this->settings['site_title'],base_url('/'),$specific_title,$btn_name);
			$sub_to_admin = 'User Contact us detail from '.$this->settings['site_title'];

			$result1 = sendEmail($this->settings['admin_email'],$this->settings['admin_email'],$sub_to_admin,$msg_to_admin);

			$text_to_user = "<p>Thank you, Your enquiry has been submitted.</p>";
			$msg_to_user = customTemplate(base_url('/'),$site_logo_lnk,$text_to_user,$this->settings['site_title'],base_url('/'),$specific_title,$btn_name);
			$sub_to_user = 'Your Contact us detail submission';
			$result2 = sendEmail($this->settings['admin_email'],$insert['email'],$sub_to_user,$msg_to_user);

			if(($result1 == "scs") && ($result2 == "scs")) {
				$this->session->set_flashdata('success','Your contact detail submitted successfully.');
			}else{
				$this->session->set_flashdata('failed','There is a problem with send email.Try again later.');
			}

		}

		$this->data['page']="home/contact";
		$this->data['page_title']="Contact Us";
		$this->data['page_name']="contact";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

}
