<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	private $settings = array();
	private $data = array();
	private $dateTimeNow;

	public function __construct()
	{
		parent::__construct();
		$this->settings = site_settings();
		$this->data['settings'] = $this->settings;
		$this->dateTimeNow = date('Y-m-d H:i:s');

		if(!$this->Auth_model->is_loggedIn()){
			return redirect('auth/login');
		}
	}

	public function index(){
		$this->data['page']="user/index";
		$this->data['page_title']="Dashboard";
		$this->data['page_name']="dashboard";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function profile(){

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){
			$update = array(
				'firstname'=>$this->input->post('f_name',true),
				'lastname'=>$this->input->post('l_name',true),
				'country'=>$this->input->post('ctry_name',true),
				'state'=>$this->input->post('st_name',true),
				'city'=>$this->input->post('cty_name',true),
				'zipcode'=>$this->input->post('zip_code',true),
				'phone_code'=>$this->input->post('phonecode',true),
				'phone'=>$this->input->post('ctc_number',true),
				'website'=>$this->input->post('web',true),
				'fb_url'=>$this->input->post('fburl',true),
				'twitter_url'=>$this->input->post('twturl',true),
				'lnkdin_url'=>$this->input->post('lnkdinurl',true),
				'chngdt'=>$this->dateTimeNow,
				'recent_ipaddr'=>''
			);

			$this->db->where('id',$this->session->userdata('eqty_userid'))->update('register',$update);
			if ($this->db->affected_rows() > 0){
				$this->session->set_flashdata('update_profile_success',"Profile Updated successfully!");
			}else{
				$this->session->set_flashdata('update_profile_failed',"Profile not updated");
			}
		}

		$this->data['page']="user/profile";
		$this->data['page_title']="User Profile";
		$this->data['page_name']="profile";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function companyProfile(){

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){

			$supporting_docs = array();
			if (!empty($_FILES['upload']['name'])){

				// Count total files
				$count_files = count($_FILES['upload']['name']);

				// Looping all files
				for($i=0;$i<$count_files;$i++) {
					$filename = time() . "_" . $_FILES['upload']['name'][$i];
					// Upload file
					if (move_uploaded_file($_FILES['upload']['tmp_name'][$i], 'uploads/' . $filename)){
						array_push($supporting_docs,'uploads/' . $filename);
					}
				}

			}
			$supporting_docs = json_encode($supporting_docs);

			$fields = array(
				'name'=>$this->input->post('name_of_company',true),
				'legal_name'=>$this->input->post('legal_name',true),
				'business_status'=>$this->input->post('business_status',true),
				'tin'=>$this->input->post('tin',true),
				'address'=>$this->input->post('address',true),
				'city'=>$this->input->post('city',true),
				'country'=>$this->input->post('country',true),
				'hq'=>$this->input->post('hq',true),
				'commencement_date'=>$this->input->post('commencement_date',true),
				'sector'=>$this->input->post('sector',true),
				'stage'=>$this->input->post('stage',true),
				'email'=>$this->input->post('email',true),
				'telephone'=>$this->input->post('telephone',true),
				'website'=>$this->input->post('website',true),
				'bankers'=>$this->input->post('bankers',true),
				'incorporation_date'=>$this->input->post('incorporation_date',true),
				'about'=>$this->input->post('about',true),
				'mission'=>$this->input->post('mission',true),
				'vision'=>$this->input->post('vision',true),
				'facebook'=>$this->input->post('facebook',true),
				'linkedin'=>$this->input->post('linkedin',true),
				'google'=>$this->input->post('google',true),
				'twitter'=>$this->input->post('twitter',true),
				'supporting_docs'=>$supporting_docs,
				'user_id'=>$this->session->userdata('eqty_userid'),
				'updated_on'=>$this->dateTimeNow,
			);

			if ($this->db->where('user_id',$this->session->userdata('eqty_userid'))->get('companies_profile')->num_rows() > 0){
				$this->db->where('user_id',$this->session->userdata('eqty_userid'))->update('companies_profile',$fields);

				if ($this->db->affected_rows() > 0){
					$this->session->set_flashdata('company_profile_success',"Company Profile Updated successfully!");
				}else{
					$this->session->set_flashdata('company_profile_failed',"Company Profile not updated");
				}

			}else{
				if ($this->db->insert('companies_profile',$fields)){
					$this->session->set_flashdata('company_profile_success',"Company Profile Updated successfully!");
				}else{
					$this->session->set_flashdata('company_profile_failed',"Company Profile not updated");
				}
			}
		}

		$this->data['page']="user/company-profile";
		$this->data['page_title']="Company Profile";
		$this->data['page_name']="company-profile";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function companyDirectors(){

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){
			$field = array(
				'title'=>$this->input->post('title',true),
				'first_name'=>$this->input->post('first_name',true),
				'surname'=>$this->input->post('surname',true),
				'dob'=>$this->input->post('dob',true),
				'resident_address'=>$this->input->post('resident_address',true),
				'resident_city'=>$this->input->post('resident_city',true),
				'resident_country_citizenship'=>$this->input->post('resident_country',true),
				'tin'=>$this->input->post('tin',true),
				'email'=>$this->input->post('email',true),
				'telephone'=>$this->input->post('tel',true),
				'highest_education'=>$this->input->post('highest_education',true),
				'school'=>$this->input->post('school',true),
				'course_name'=>$this->input->post('course_name',true),
				'completion_date'=>$this->input->post('completion_date',true),
				'past_organisation'=>$this->input->post('past_organisation',true),
				'job_title'=>$this->input->post('job_title',true),
				'start_date'=>$this->input->post('start_date',true),
				'end_date'=>$this->input->post('end_date',true),
				'linkedin'=>$this->input->post('linkedin',true),
				'google'=>$this->input->post('google',true),
				'twitter'=>$this->input->post('twitter',true),
				'created_on'=>$this->dateTimeNow,
				'user_id'=>$this->session->userdata('eqty_userid'),
			);

			if ($this->input->post('action',true) == "edit"){
				$this->db->where('user_id',$this->session->userdata('eqty_userid'))
					->where('id',$this->input->post('id',true))->update('directors',$field);

				if ($this->db->affected_rows() > 0){
					$this->session->set_flashdata('crud_director_success',"Director updated successfully!");
				}else{
					$this->session->set_flashdata('crud_director_failed',"Director not updated,retry");
				}
			}else{
				if($this->db->insert('directors',$field)){
					$this->session->set_flashdata('crud_director_success',"Director created successfully!");
				}else{
					$this->session->set_flashdata('crud_director_failed',"Director not created,retry");
				}
			}
		}

		$this->data['page']="user/company-directors";
		$this->data['page_title']="Company Directors";
		$this->data['page_name']="company-directors";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function companyManagement(){

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){
			$field = array(
				'title'=>$this->input->post('title',true),
				'first_name'=>$this->input->post('first_name',true),
				'surname'=>$this->input->post('surname',true),
				'dob'=>$this->input->post('dob',true),
				'resident_address'=>$this->input->post('resident_address',true),
				'resident_city'=>$this->input->post('resident_city',true),
				'resident_country_citizenship'=>$this->input->post('resident_country',true),
				'tin'=>$this->input->post('tin',true),
				'email'=>$this->input->post('email',true),
				'telephone'=>$this->input->post('tel',true),
				'highest_education'=>$this->input->post('highest_education',true),
				'school'=>$this->input->post('school',true),
				'course_name'=>$this->input->post('course_name',true),
				'completion_date'=>$this->input->post('completion_date',true),
				'past_organisation'=>$this->input->post('past_organisation',true),
				'job_title'=>$this->input->post('job_title',true),
				'start_date'=>$this->input->post('start_date',true),
				'end_date'=>$this->input->post('end_date',true),
				'linkedin'=>$this->input->post('linkedin',true),
				'google'=>$this->input->post('google',true),
				'twitter'=>$this->input->post('twitter',true),
				'created_on'=>$this->dateTimeNow,
				'user_id'=>$this->session->userdata('eqty_userid'),
			);

			if ($this->input->post('action',true) == "edit"){
				$this->db->where('user_id',$this->session->userdata('eqty_userid'))
					->where('id',$this->input->post('id',true))->update('managements',$field);

				if ($this->db->affected_rows() > 0){
					$this->session->set_flashdata('crud_management_success',"Management updated successfully!");
				}else{
					$this->session->set_flashdata('crud_management_failed',"Management not updated,retry");
				}
			}else{
				if($this->db->insert('managements',$field)){
					$this->session->set_flashdata('crud_management_success',"Management created successfully!");
				}else{
					$this->session->set_flashdata('crud_management_failed',"Management not created,retry");
				}
			}
		}

		$this->data['page']="user/company-management";
		$this->data['page_title']="Company Management";
		$this->data['page_name']="company-management";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function pastInvestors(){

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){
			$field = array(
				'first_name'=>$this->input->post('first_name',true),
				'surname'=>$this->input->post('surname',true),
				'dob'=>$this->input->post('dob',true),
				'resident_address'=>$this->input->post('resident_address',true),
				'resident_city'=>$this->input->post('resident_city',true),
				'resident_country_citizenship'=>$this->input->post('resident_country',true),
				'tin'=>$this->input->post('tin',true),
				'investment_type'=>$this->input->post('investment_type',true),
				'no_of_shares'=>$this->input->post('no_of_shares',true),
				'value_of_shares'=>$this->input->post('value_of_shares',true),
				'created_on'=>$this->dateTimeNow,
				'user_id'=>$this->session->userdata('eqty_userid'),
			);

			if ($this->input->post('action',true) == "edit"){
				$this->db->where('user_id',$this->session->userdata('eqty_userid'))
					->where('id',$this->input->post('id',true))->update('past_investors',$field);

				if ($this->db->affected_rows() > 0){
					$this->session->set_flashdata('crud_investor_success',"Past Investor updated successfully!");
				}else{
					$this->session->set_flashdata('crud_investor_failed',"Past Investor not updated,retry");
				}
			}else{
				if($this->db->insert('past_investors',$field)){
					$this->session->set_flashdata('crud_investor_success',"Past Investor created successfully!");
				}else{
					$this->session->set_flashdata('crud_investor_failed',"Past Investor not created,retry");
				}
			}
		}

		$this->data['page']="user/company-past-investors";
		$this->data['page_title']="User Profile";
		$this->data['page_name']="company-past-investors";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function delete($who,$id){
		switch ($who){
			case 'past-investor':
				$this->db->where('user_id',$this->session->userdata('eqty_userid'))->where('id',$id)->delete('past_investors');
				break;
			case 'management':
				$this->db->where('user_id',$this->session->userdata('eqty_userid'))->where('id',$id)->delete('managements');
				break;
			case 'directors':
				$this->db->where('user_id',$this->session->userdata('eqty_userid'))->where('id',$id)->delete('directors');
				break;
			case 'announcement':
				$this->db->where('user_id',$this->session->userdata('eqty_userid'))->where('id',$id)->delete('company_announcements');
			case 'financial-statements':
				$this->db->where('user_id',$this->session->userdata('eqty_userid'))->where('id',$id)->delete('financial_statements');

		}

		return redirect($this->agent->referrer());
	}

	public function campaign(){
		$this->data['page']="user/campaign";
		$this->data['page_title']="User Campaign";
		$this->data['page_name']="campaign";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function pledges(){
		$this->data['page']="user/pledges";
		$this->data['page_title']="Pledges received";
		$this->data['page_name']="pledges";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function backedCampaigns(){
		$this->data['page']="user/backed-campaigns";
		$this->data['page_title']="Backed Campaign";
		$this->data['page_name']="backed-campaigns";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function payments(){
		$this->data['page']="user/payments";
		$this->data['page_title']="Payments";
		$this->data['page_name']="payments";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function myInvestments(){
		$this->data['page']="user/my-investments";
		$this->data['page_title']="My Investments";
		$this->data['page_name']="myInvestments";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function announcements(){

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){
			$field = array(
				'content'=>$this->input->post('content',true),
				'user_id'=>$this->session->userdata('eqty_userid'),
				'status'=>$this->input->post('status',true),
				'updated_on'=>$this->dateTimeNow
			);

			if ($this->input->post('action',true) == "edit"){
				$this->db->where('user_id',$this->session->userdata('eqty_userid'))
					->where('id',$this->input->post('id',true))->update('company_announcements',$field);

				if ($this->db->affected_rows() > 0){
					$this->session->set_flashdata('crud_announcements_success',"Announcement updated successfully!");
				}else{
					$this->session->set_flashdata('crud_announcements_failed',"Announcement not updated,retry");
				}
			}else{
				if($this->db->insert('company_announcements',$field)){
					$this->session->set_flashdata('crud_announcements_success',"Announcement created successfully!");
				}else{
					$this->session->set_flashdata('crud_announcements_failed',"Announcement not created,retry");
				}
			}

		}

		$this->data['page']="user/announcements";
		$this->data['page_title']="Announcements";
		$this->data['page_name']="announcements";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function settings(){
		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {

			if ( !empty($this->input->post('bank_det',true)) ) {
				$field = array(
					'acc_num' => $this->input->post('acct_num', true),
					'acc_name' => $this->input->post('acct_name', true),
					'ifsc' => $this->input->post('ifsc_code', true)
				);
				$this->db->where('id',$this->session->userdata('eqty_userid'))->update('register',$field);
				if ($this->db->affected_rows() > 0){
					$this->session->set_flashdata('setting_success',"Account details updated successfully!");
				}else{
					$this->session->set_flashdata('setting_failed',"Account details not updated,retry");
				}
			}

			if ( !empty($this->input->post('account_password',true)) ) {

				$user_result = $this->db->where(array('id'=>$this->session->userdata('eqty_userid'),
					'repassword'=>md5($this->input->post('old_pass',true))))
					->get('register');

				if ($user_result->num_rows() > 0){

					if ( $this->input->post('new_pass',true) == $this->input->post('confirm_pass',true) ){

						$this->db->where('id',$this->session->userdata('eqty_userid'))->update('register',array('password'=>$this->input->post('new_pass',true),'repassword'=>md5($this->input->post('new_pass',true))));

						if ($this->db->affected_rows() > 0){
							$this->session->set_flashdata('setting_success',"Password updated successfully!");
						}else{
							$this->session->set_flashdata('setting_failed',"Password not updated,retry");
						}

					}else{
						$this->session->set_flashdata('setting_failed',"New password do not match!");
					}

				}else{
					$this->session->set_flashdata('setting_failed',"Old password is incorrect!");
				}

			}

		}
		$this->data['page']="user/settings";
		$this->data['page_title']="Settings";
		$this->data['page_name']="settings";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function financialStatements(){

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
			$docs = array();
			if (!empty($_FILES['file']['name'])){

				// Count total files
				$count_files = count($_FILES['file']['name']);

				// Looping all files
				for($i=0;$i<$count_files;$i++) {
					$filename = time() . "_" . $_FILES['file']['name'][$i];
					// Upload file
					if (move_uploaded_file($_FILES['file']['tmp_name'][$i], 'uploads/' . $filename)){
						array_push($docs,'uploads/' . $filename);
					}
				}

			}
			$docs_ = json_encode($docs);

			$array = array(
				'user_id'=>$this->session->userdata('eqty_userid'),
				'title'=>$this->input->post('title', true),
				'remarks'=>$this->input->post('remarks', true),
				'created_on'=>$this->dateTimeNow
			);

			if ($this->input->post('action',true) == "edit"){
				if (!empty($docs) && count($docs) > 0){
					$array['document']=$docs_;
				}

				$this->db->where('user_id',$this->session->userdata('eqty_userid'))
							->where('id',$this->input->post('id',true))
							->update('financial_statements',$array);
				if ($this->db->affected_rows() > 0){
					$this->session->set_flashdata('crud_fd_success',"Financial Statement updated successfully!");
				}else{
					$this->session->set_flashdata('crud_fd_failed',"Financial Statement could not be updated,retry");
				}

			}else{
				$array['document']=$docs_;
				if ($this->db->insert('financial_statements',$array)){
					$this->session->set_flashdata('crud_fd_success',"Financial Statement submitted successfully!");
				}else{
					$this->session->set_flashdata('crud_fd_failed',"Financial Statement could not be submitted");
				}
			}
		}
		$this->data['page']="user/financial-statements";
		$this->data['page_title']="Financial Statements";
		$this->data['page_name']="financial-statements";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

	public function campaignsIFollow(){
		$this->data['page']="user/following_campaigns";
		$this->data['page_title']="Campaigns I Follow";
		$this->data['page_name']="following-campaigns";
		$this->data['data']=$this->data;

		$this->load->view('home/layout/app',$this->data);
	}

}
