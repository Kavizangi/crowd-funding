<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model{
	public function is_loggedIn() {
		if( !empty($this->session->userdata('eqty_mail')) && !empty($this->session->userdata('eqty_userid')) ){
			return true;
		}
		return false;
	}

	public function getLoggedInUserData(){
		if ($this->is_loggedIn()){
			return $this->db->where('id',$this->session->userdata('eqty_userid'))->get('register')->row();
		}
		return [];
	}
}
