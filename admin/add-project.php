<?php
require "includes/header.php";

$isEdit=false;
if(isset($prjt)) $isEdit=true;

/** process if the form has been submitted **/
if(isset($prjt_sub)) {
	$set="user_id=:user_id";
	$set.=",title=:title";
	$set.=",introduction=:introduction";
	$set.=",problem=:problem";
	$set.=",solution=:solution";
	$set.=",intended_results=:intended_results";
	$set.=",accomplishment_so_far=:accomplishment_so_far";
	$set.=",revenue_strategy=:revenue_strategy";
	$set.=",use_of_funds=:use_of_funds";
	$set.=",investment_terms=:investment_terms";
	$set.=",city=:city";
	$set.=",country=:country";
	$set.=",category=:category";
	$set.=",raise_amount=:raise_amount";
	$set.=",duration=:duration";
	$set.=",end_date=:end_date";
	$set.=",equity_offered=:equity_offered";
	$set.=",updated_on=:updated_on";

	$images_uploaded = array();

	if ( !empty($_FILES["file"]) && count($_FILES["file"]["name"]) >  0 ) {

		for ($i=0; $i < count($_FILES["file"]["name"]); $i++) {

			$target_dir = "../uploads/campaigns";
			$target_dir1 = "uploads/campaigns";
			$target_file = $target_dir . time() . basename($_FILES["file"]["name"][$i]);
			$target_file1 = $target_dir1 . time() . basename($_FILES["file"]["name"][$i]);
			$file_type = $_FILES["file"]["type"][$i];
			$post_tmp = $_FILES["file"]["tmp_name"][$i];

			switch ($file_type) {
				case 'image/jpeg':
				case 'image/gif':
				case 'image/png':

					if (is_dir($target_dir) && is_writable($target_dir)) {

						if (move_uploaded_file($post_tmp, $target_file)) {
							array_push($images_uploaded, $target_file1);
						}

					}
			}

		}

	}

	if($isEdit) {
		if (!empty($images_uploaded)) {
			$set .= ",images=:images";
		}
		$que="update campaigns set $set where id='$prjt'";
		$db->query($que);

		if (!empty($images_uploaded)) {
			$db->bind(":images", json_encode($images_uploaded));
		}
	}else {

		$set.=",status=:status";
		$set.=",created_on=:created_on";
		$set.=",images=:images";
		$que="insert into campaigns set $set";
		$db->query($que);

		$db->bind(":status", 'pending');
		$db->bind(":created_on", $timestamp);
		$db->bind(":images", json_encode($images_uploaded));
	}

	$created_on = time();
	$end_date_ = strtotime($end_date);

	$date_diff = $end_date_ - $created_on;
	$days = round($date_diff / (60 * 60 * 24));

	$db->bind(":user_id", $post_by);
	$db->bind(":title", $title);
	$db->bind(":introduction", $introduction);
	$db->bind(":problem", $problem);
	$db->bind(":solution", $solution);
	$db->bind(":intended_results", $intended_results);
	$db->bind(":accomplishment_so_far", $accomplishment_so_far);
	$db->bind(":revenue_strategy", $revenue_strategy);
	$db->bind(":use_of_funds", $use_of_funds);
	$db->bind(":investment_terms", $investment_terms);
	$db->bind(":city", $city);
	$db->bind(":country", $country);
	$db->bind(":category", $category);
	$db->bind(":raise_amount", $raise_amount);
	$db->bind(":duration", $days);
	$db->bind(":end_date", $end_date);
	$db->bind(":equity_offered", $equity_offered);
	$db->bind(":updated_on", $timestamp);

	$exec = $db->execute();
	if ($db->lastInsertId() != 0){
		$extra->redirect_to($baseUrl."project/");
	}else if ($exec && $isEdit){
		$extra->setMsg("Project updated successfully!", "success");
	}

}

if($isEdit) {
	$field_req = '';
	$db->query("select * from campaigns where id=:id");
	$db->bind(":id", $prjt);
	$result=$db->fetch();
	if(!empty($result)) extract($result);
	else $extra->redirect_to($baseUrl."project/");
}

?>

<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-2">
				<h3 class="content-header-title mb-0"><?php echo (isset($isEdit) && $isEdit)?"Edit":"Create"; ?> Project/Campaign</h3>
			</div>
		</div>
		<div class="content-body">
			<section id="html">
				<div class="row">
					<div class="col-xs-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Fund Raiser Details </h4>
								<?php echo $extra->flashMsg(); ?>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
							</div>
							<div class="card-body collapse in">
								<div class="card-block card-dashboard table-responsive">
									<form name="ad_prjtpost" action="" class="form-horizontal" method="post" enctype="multipart/form-data">
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">Posted By </label>
											<div class="col-sm-10">
												<select id="postId" class="form-control group caps" name="post_by" onChange="return select(this.value)">
													<option value="0"> Select User </option>
													<?=$drop->dropselectSingle("select id,firstname from register where active_status='1' and email_active_status='1' order by firstname asc",$user_id);?>
												</select>
											</div>
										</div>

										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">Campaign Title *</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" value="<?php echo isset($title) ? $title : '';?>" id="title" name="title" placeholder="" required>
											</div>
											</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">Introduction *</label>
											<div class="col-sm-10">
												<textarea rows="4" class="tinymce form-control" id="introduction" name="introduction" placeholder="Write here.."><?php echo isset($introduction) ? $introduction : "";?></textarea>
											</div>
										</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">The Problem *</label>
											<div class="col-sm-10">
												<textarea rows="4" class="tinymce form-control" id="problem" name="problem" placeholder="Write here.."><?php echo isset($problem) ? $problem : '';?></textarea>
											</div>
										</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">The Solution *</label>
											<div class="col-sm-10">
												<textarea rows="4" class="tinymce form-control" id="solution" name="solution" placeholder="Write here.."><?php echo isset($solution) ? $solution : '';?></textarea>
											</div>
										</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">Intended Results *</label>
											<div class="col-sm-10">
												<textarea rows="4" class="tinymce form-control" id="intended_results" name="intended_results" placeholder="Write here.."><?php echo isset($intended_results) ? $intended_results : '';?></textarea>
											</div>
										</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">Accomplishment So far *</label>
											<div class="col-sm-10">
												<textarea rows="4" class="tinymce form-control" id="accomplishment_so_far" name="accomplishment_so_far" placeholder="Write here.."><?php echo isset($accomplishment_so_far) ? $accomplishment_so_far : '';?></textarea>
											</div>
										</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">Revenue Strategy *</label>
											<div class="col-sm-10">
												<textarea rows="4" class="tinymce form-control" id="revenue_strategy" name="revenue_strategy" placeholder="Write here.."><?php echo isset($revenue_strategy) ? $revenue_strategy : '';?></textarea>
											</div>
										</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">Use of Funds *</label>
											<div class="col-sm-10">
												<textarea rows="4" class="tinymce form-control" id="use_of_funds" name="use_of_funds" placeholder="Write here.."><?php echo isset($use_of_funds) ? $use_of_funds : '';?></textarea>
											</div>
										</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">Investment Terms *</label>
											<div class="col-sm-10">
												<select name="investment_terms" id="investment_terms" class="form-control" required>
													<option value="">Select investment terms</option>
													<option <?php echo isset($investment_terms) && trim(strtolower($investment_terms))=="loan" ? 'selected' : ''?> value="Loan">Loan</option>
													<option <?php echo isset($investment_terms) && trim(strtolower($investment_terms))=="preference shares" ? 'selected' : ''?> value="Preference Shares">Preference Shares</option>
													<option <?php echo isset($investment_terms) && trim(strtolower($investment_terms))=="common shares" ? 'selected' : ''?> value="Common Shares">Common Shares</option>
												</select>
											</div>
										</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">Campaign Image *</label>
											<div class="col-sm-10 align-items-center pt-3">
												<div class="col-12 file-container">
													<div class="row">
														<div class="col-md-10">
															<p style="color: red">1920 x 964 recommended resolution.</p>
															<input type="file" name="file[]" class="form-control" <?php echo empty($images) ? "required" : "";?>/>
														</div>
														<div class="col-md-2">
															<br/>
															<br/>
															<button class="btn btn-danger" type="button" onclick="removeFile(this)"> - </button>
														</div>
													</div>
												</div>
												<div class="col-12">
													<br/>
													<button class="btn btn-info" type="button" onclick="addAnotherFile();"> Add Another File </button>
												</div>
											</div>
											<div class="col-md-2"></div>
											<div class="col-md-10">
												<p>
													<?php $i = 1;
													if (!empty($images)) {
														foreach (json_decode($images) as $doc) {
															?>
															<a href="<? echo $doc; ?>" target="_blank"
															   class="btn btn-link"><? echo $i++ . ". View"; ?></a><br/>
															<?
														}
													}
													?>
												</p>
											</div>
										</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">City *</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" value="<?php echo isset($city) ? $city : '';?>" id="city" name="city" placeholder="City" required />
											</div>
										</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">Country *</label>
												<div class="col-sm-10">
													<select name="country" required class="form-control">
														<option value="">Select Country</option>
														<?php

														$db->query("select * from country WHERE country_status='1'" );
														$countries = $db->fetchAll();
														foreach ($countries as $item) {
															$item = (object) $item;
															?>
															<option <?php echo isset($country) && $country == $item->country_id ? 'selected' : '';?> value="<?php echo $item->country_id;?>"><?php echo $item->country_name;?></option>
															<?php
														}
														?>
													</select>
												</div>
										</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">Campaign Category *</label>
											<div class="col-sm-10">
												<select name="category" class="form-control" id="category" required>
													<option value="">Select a Category</option>
													<?php
													$db->query("select * from category WHERE active_status='1'" );
													$categories = $db->fetchAll();
													foreach ($categories as $item) {
														$item = (object) $item;
														?>
														<option <?php echo isset($category) && $category == $item->id ? 'selected' : '';?> value="<?php echo $item->id;?>"><?php echo $item->catagory_name;?></option>
														<?php
													}
													?>
												</select>
											</div>
										</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">How much do you want to raise? *</label>
											<div class="col-sm-10">
												<input type="number" class="form-control" id="raise_amount" value="<?php echo isset($raise_amount) ? $raise_amount : '';?>" name="raise_amount" placeholder="">
											</div>
										</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">Equity offered(%) *</label>
											<div class="col-sm-10">
												<input type="number" class="form-control" id="equity_offered" value="<?php echo isset($equity_offered) ? $equity_offered : '';?>" name="equity_offered" placeholder="">
											</div>
										</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">Campaign End Date *</label>
											<div class="col-sm-10">
												<input type="date" id="end_date" class="form-control" value="<?php echo isset($end_date) ? $end_date : '';?>" name="end_date">
											</div>
										</div>

										<div class="form-actions center col-sm-12">
											<button type="submit" class="btn btn-primary mr-1" name="prjt_sub"><i class="fa fa-check-square-o"></i> Save</button>
											<button type="button" class="btn btn-warning" onclick="window.history.back();"><i class="ft-x"></i> Cancel</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
<script>
	function addAnotherFile() {
		$(".file-container").append('<div class="row" style="margin-top: 10px;"> <div class="col-md-10"><p style="color: red">570 x 350 recommended resolution.</p><input type="file" name="file[]" class="form-control"/></div><div class="col-md-2"><button class="btn btn-danger" type="button" onclick="removeFile(this)"> - </button></div></div>');
	}

	function removeFile(e) {
		$(e).parent().parent().remove();
	}

</script>
<?php
require "custom.php";
require "includes/footer.php";
?>
<script>
	$( "#end_date" ).datepicker({
		minDate: 0,
		dateFormat: 'yy-mm-dd'
	});
</script>
