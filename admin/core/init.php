<?php
// error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

ob_start();
//session_save_path("/home/thavasu/tmp");
//session_save_path(dirname(dirname(__FILE__))."/tmp");
//ini_set('session.gc_probability',1);
date_default_timezone_set('Europe/Lisbon');
session_start();

define("BASEPATH","");
define("ENVIRONMENT","development");
//include codeigniter db configuration
require "../application/config/database.php";
$c_db = $db['default'];

/** load required files **/
require "database.php";
require "resize.php";
require "mailer/PHPMailerAutoload.php";

/** database configuration **/
define("DB_HOST", $c_db['hostname']);
define("DB_USER", $c_db['username']);
define("DB_PASS", $c_db['password']);
define("DB_NAME", $c_db['database']);
$db=new database();

/** options loader **/
require "options.php";

/** pagination **/
require "pagination.php";

/** custom-get current page **/
$current_file = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']).'.php';
$livepage = $current_file;
