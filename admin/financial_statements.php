<?php
require "includes/header.php";
?>

	<div class="app-content content container-fluid">
		<div class="content-wrapper">
			<div class="content-header row">
				<div class="content-header-left col-md-6 col-xs-12 mb-2">
					<h3 class="content-header-title mb-0">Review Financial Statements</h3>
				</div>
			</div>
			<div class="content-body">
				<section id="html">
					<div class="row">
						<div class="col-xs-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Financial Statements</h4>
									<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
									<div class="heading-elements">
										<ul class="list-inline mb-0">
											<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
											<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="card-body collapse in">
									<div class="card-block card-dashboard table-responsive">
										<?php $extra->flashMsg(); ?>
										<table id="datatable" class="table table-striped table-bordered sourced-data">
											<thead>
											<tr>
												<th>#</th>
												<th>Title</th>
												<th>Remarks</th>
												<th>Document</th>
												<th>Review Comment</th>
												<th>Reviewed On</th>
												<th>Created On</th>
												<th>Action</th>
											</tr>
											</thead>
											<tbody>
											<?php
											$i=1;
											$db->query("select * from financial_statements order by id desc");
											$result = $db->fetchAll();
											foreach($result as $row):
												$row = (object) $row;
												$docs = json_decode($row->document);
											?>
												<tr>
													<td><?php echo $i++;?></td>
													<td><?php echo $row->title;?></td>
													<td><?php echo $row->remarks;?></td>
													<td>
														<?php $j=1;
														foreach ($docs as $doc) {
															?>
															<p>
																<a href="<?php echo "../".$docs;?>" class="btn btn-link"><?php echo $j.'.';?> View</a>
															</p>
															<?php
															$i++;
														}
														?>
													</td>
													<td><?php echo $row->review_comment;?></td>
													<td><?php echo $row->reviewed_on;?></td>
													<td><?php echo $row->created_on;?></td>
													<td>
														<button data-toggle="modal" data-target="#financialStatementsModal<?php echo $i;?>"  class="btn btn-sm btn-icon btn-info"><i class="fa fa-pencil-square"></i></button>

														<div class="modal fade" id="financialStatementsModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="financialStatementsModalLabel" aria-hidden="true">
															<div class="modal-dialog modal-lg" role="document">
																<div class="modal-content">
																	<div class="modal-header">
																		<h5 class="modal-title" id="exampleModalLabel">Upload Financial Statement</h5>
																		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																			<span aria-hidden="true">&times;</span>
																		</button>
																	</div>
																	<div class="modal-body">
																		<div class="row">
																			<div class="col-md-12">
																				<form method="post" enctype="multipart/form-data" action="">

																					<div class="col-md-12">
																						<div class="field">
																							<label for="title">Title</label>
																							<input class="form-control" value="<?php echo $row->title;?>" id="title" name="title" required/>
																						</div>
																					</div>

																					<div class="col-md-12">
																						<div class="field">
																							<label for="content">Remarks</label>
																							<textarea id="content" name="remarks"><?php echo $row->remarks;?></textarea>
																						</div>
																					</div>

																					<div class="col-md-12">
																						<input type="file" name="file[]" class="form-control"/>
																						<br/>
																						<?php $i=1;
																						foreach ($docs as $doc) {
																							?>
																							<p>
																								<a href="<?php echo base_url($docs)?>" class="btn btn-link"><?php echo $i.'.';?> View</a>
																							</p>
																							<?php
																							$i++;
																						}
																						?>
																						<br/>
																					</div>

																					<div class="col-md-12">
																						<input type="hidden" name="action" value="edit"/>
																						<input type="hidden" name="id" value="<?php echo $row->id;?>"/>
																						<button type="submit" name="save_action" class="btn-primary">Update Changes</button>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>

													</td>
												</tr>
												<?php $i++; endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
<?php require "includes/footer.php"; ?>
